$(document).ready(function(){

    $('.generateImage').on('click', function(e){

        $button = $(this);
        $button.html('<img src="../img/loader.gif" style="height: 14px; width: 14px; margin-right: 5px;">Working...');


        $tr        = $(this).closest("tr");
        var url    = $(this).data('href');
        var source = new EventSource(url);
        var countImage = "";

        source.onmessage = function(e) {
            var obj = JSON.parse(e.data);
            console.log("obj = " + obj["time"]);

            if ( obj["time"] != undefined )
            {
                console.log("obj = " + obj["time"]);
            }
            else if ( obj["message"] != undefined )
            {
                console.log("obj = " + obj["message"]);
                $button.html('<i class="icon-check" style="margin-right: 5px; color:green"></i>Done');
                $.growl.notice({ message: "Image successful optimized." });
            }
            else if ( obj["count"] != undefined )
            {
                countImage = obj["count"];
                console.log(countImage);
            }
            else if ( obj["error"] != undefined )
            {
                console.log("obj = " + obj["error"]);
                $button.html('<i class="icon-exclamation-sign" style="margin-right: 5px; color:red"></i>Fail');
                $.growl.error({ message: "An error occurred in core module: "+resp.error });
                source.close();
            }
            else if ( obj["finish"] != undefined )
            {
                console.log("obj = " + obj["finish"]);

                var weigthbefore = $tr.find('td:nth-child(5)').text() * 1;
                var rate = Math.round((parseInt(obj["dest_size"])*1 / parseInt(obj["src_size"])*1 * 100) * 100) / 100 ;
                $tr.find('td:nth-child(6)').replaceWith('<td class="" style="background: palegreen;">'+obj["dest_size"]+'</td>');
                $tr.find('td:nth-child(7)').replaceWith('<td class="" style="background: palegreen;">'+rate.toPrecision(2)+'%</td>');

                $button.html('<i class="icon-check" style="margin-right: 5px; color:green"></i>Done');
                $.growl.notice({ message: "Optimization finish"});
                source.close();
            }
            else if ( obj["key"] != undefined )
            {
                console.log("obj = " + obj["key"]);
                $button.html('<img src="../img/loader.gif" style="height: 14px; width: 14px; margin-right: 5px;">Optimize : ' + obj["key"] + ' / ' + countImage);
            }
        };

        source.onerror = function(event) {
            console.log("server close");
            $button.html('<i class="icon-exclamation-sign" style="margin-right: 5px; color:red"></i>Fail');
            $.growl.error({ message: "An error occurred during server connection." });
            source.close();
        }
    });
});