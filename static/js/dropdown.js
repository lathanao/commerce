// check if the page have dropdwon menu

// https://codepen.io/iukones/pen/vYORObo
// https://codepen.io/piya50/pen/ExVePEK


var dropdown = document.getElementsByClassName('dropdown');

if (dropdown.length >= 1) {

    for (let i = 0; i < dropdown.length; i++) {
        const item = dropdown[i];

        var menu,btn,overflow;
        
        item.addEventListener('click' , function(){            
            
            console.log('click action row ss')
            
            for (let i = 0; i < this.children.length; i++) {
                const e = this.children[i];

                if (e.classList.contains('menu')) {
                    menu = e;                  
                }else if (e.classList.contains('button')) {
                    btn = e;
                }else if (e.classList.contains('menu-overflow')) {
                    overflow = e;
                }             
            }
            if (menu.classList.contains('opacity-0')) {
                menu.classList.remove('ease-in'); 
                menu.classList.remove('duration-75'); 
                menu.classList.add('ease-out');
                menu.classList.add('duration-100');
    
                menu.classList.remove('opacity-0');
                menu.classList.remove('scale-95');  
                menu.classList.add('opacity-100');   
                menu.classList.add('scale-100');
             
                setTimeout(function(){ menu.classList.toggle("hidden"); }, 100);
            }else{
                menu.classList.remove('ease-in'); 
                menu.classList.remove('duration-100');
                menu.classList.add('ease-out');
                menu.classList.add('duration-75');
    
                menu.classList.remove('opacity-100'); 
                menu.classList.remove('scale-100');
                menu.classList.add('opacity-0');    
                menu.classList.add('scale-95'); 
                
                setTimeout(function(){ menu.classList.toggle("hidden"); }, 75);
            }      
        });        
        
        document.onkeydown = function(evt) {
            evt = evt || window.event
            var isEscape = false
            if ("key" in evt) {
                isEscape = (evt.key === "Escape" || evt.key === "Esc")
            } else {
                isEscape = (evt.keyCode === 27)
            }
            if (isEscape && document.body.classList.contains('modal-active')) {
                toggleModal()
            }
        };

        var showMenu = function(){
            menu.classList.remove('ease-in'); 
            menu.classList.remove('duration-75'); 
            menu.classList.add('ease-out');
            menu.classList.add('duration-100');
            

            menu.classList.remove('opacity-0');
            menu.classList.remove('scale-95');  
            menu.classList.add('opacity-100');   
            menu.classList.add('scale-100');  
            
            menu.classList.toggle("hidden");
        };

        var hideMenu = function(){
            menu.classList.remove('ease-in'); 
            menu.classList.remove('duration-100');
            menu.classList.add('ease-out');
            menu.classList.add('duration-75');

            menu.classList.remove('opacity-100'); 
            menu.classList.remove('scale-100');
            menu.classList.add('opacity-0');    
            menu.classList.add('scale-95');

            menu.classList.toggle("hidden");   
        };        
    }    
};