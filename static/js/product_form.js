
window.onload = () => {


    console.log('readURL');

    let fileupload = document.getElementById('file-upload');
    let imageuploadwrap = document.getElementById('image-upload-wrap');

    if (!fileupload || !imageuploadwrap) {
      return
    }

    fileupload.addEventListener('change' , function(){

        //console.log('change')
        //console.log(this)
        //console.log(this.files)
        if (this.files && this.files[0]) {

            console.log('if')
            let reader = new FileReader();
  
            reader.onload = function(e) {

                //console.log(e)
                //console.log(e.target)
                //console.log(e.target.result)

                var elem = document.createElement('div');
                elem.className = 'shadow h-32 w-32 font-medium bg-white rounded-md flex items-center justify-center';
                var image = document.createElement('img');
                image.setAttribute('src', e.target.result);
                elem.append(image)
                imageuploadwrap.append(elem)

                console.log(e.target)
              
            };
            console.log("----------- THIS FILE")
            console.log(this.files[0])
            reader.readAsDataURL(this.files[0]);

            
            const formData = new FormData();

            console.log("----------- FORM DATA")
            formData.append('username', 'abc123');
            formData.append('image', this.files[0]);
            
            console.log("----------- FETCH")
            fetch('http://localhost:8081/catalog/product/image/upload', {
              method: 'POST',
              body: formData
            })
            .then(response => response.json())
            .then(result => {
              console.log('Success:', result);
            })
            .catch(error => {
              console.error('Error:', error);
            });

            // console.log("------------ SSE")
            // const sse = new EventSource('http://localhost:8081/catalog/product/image/stream');

            /* This will listen only for events
             * similar to the following:
             *
             * event: notice
             * data: useful data
             * id: someid
             *
             */
            // console.log("sse.addEventListener")
            // sse.addEventListener("notice", function(e) {
            //   console.log(e.data)
            // })
          
            // /* Similarly, this will listen for events
            //  * with the field `event: update`
            //  */
            // sse.addEventListener("update", function(e) {
            //   console.log(e.data)
            // })
          
            // sse.addEventListener("event", function(e) {
            //     console.log(e.data)
            // })

            // sse.addEventListener("data", function(e) {
            //     console.log("----------- DATA")
            //     console.log(e.data)
            // })

            // /* The event "message" is a special case, as it
            //  * will capture events without an event field
            //  * as well as events that have the specific type
            //  * `event: message` It will not trigger on any
            //  * other event type.
            //  */
            // sse.addEventListener("message", function(e) {
            //     console.log("----------- MESSAGE")
            //     console.log(e.data)
            //     console.log(e.mess)
            // })
            // sse.addEventListener("done", function(e) {
            //     console.log("server close by event")
            //     sse.close();
            // })
            // sse.onmessage = function(e) {
            //     //var obj = JSON.parse(e.data);
            //     console.log("----------- ON MESSAGE")
            //     console.log(e);
    
            //     // if ( obj["event"] != undefined )
            //     // {
            //     //     console.log("obj = " + obj["event"]);
            //     // }
            // };
    
            // sse.onerror = function(e) {
            //     console.log("----------- ON ERROR")
            //     //var obj = JSON.parse(e.data);
            //     console.log(e);
            //     console.log("server close")
            //     sse.close();
            // }

        } else {
            //console.log('else')
            removeUpload();
        }

        // var dad = this.parentNode;
        // dad.classList.add('animated' , 'fadeOut');
        // setTimeout(() => {
        //     dad.remove();
        // }, 1000);
    });


    function removeUpload() {
        // $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        // $('.file-upload-content').hide();
        // $('.image-upload-wrap').show();
    }
    // $('.image-upload-wrap').bind('dragover', function () {
    //     $('.image-upload-wrap').addClass('image-dropping');
    // });
    // $('.image-upload-wrap').bind('dragleave', function () {
    //     $('.image-upload-wrap').removeClass('image-dropping');
    // });
}