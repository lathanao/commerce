module money

// Amount is a datastructure that stores the amount being used for calculations.
struct Amount  {
	val int
}

// Money represents monetary value information, stores
// currency and amount value.
struct Money  {
mut:
	amount   &Amount
	currency &Currency
}

// Injection points for backward compatibility.
// If you need to keep your JSON marshal/unmarshal way, overwrite them like below.
//   money.UnmarshalJSON = fn (mut m Money, b []byte) error { ... }
//   money.MarshalJSON = fn (m Money) ([]byte, error) { ... }
// const (
// 	// UnmarshalJSONfn is injection point of json.Unmarshaller for money.Money
// 	UnmarshalJSON = defaultUnmarshalJSON
// 	// MarshalJSONfn is injection point of json.Marshaller for money.Money
// 	MarshalJSON = defaultMarshalJSON
// )

// fn default_unmarshal_json(mut m Money, b []byte) error {
// 	data := make(map[string]interface{})
// 	err := json.Unmarshal(b, &data)
// 	if err != nil {
// 		return err
// 	}
// 	ref := new(int64(data["amount"].(float64)), data["currency"].(string))
// 	m = &ref
// 	return nil
// }

// fn default_marshal_json(m Money) ([]byte, error) {
// 	buff := bytes.NewBufferString(fmt.Sprintf(`{"amount": %d, "currency": "%s"}`, m.Amount(), m.Currency().Code))
// 	return buff.Bytes(), nil
// }


pub fn say_hi_money() {
    println('hello from money!')
}

// New creates and returns new instance of Money.
pub fn new (amount int, code string) &Money {
	return &Money{
		amount:   &Amount{val: amount},
		currency: new_currency(code).get(),
	}
}

// // Currency returns the currency used by Money.
// fn (mut m Money) Currency() *Currency {
// 	return m.currency
// }

// // Amount returns a copy of the internal monetary value as an int64.
// fn (mut m Money) Amount() int64 {
// 	return m.amount.val
// }

// // SameCurrency check if given Money is equals by currency.
// fn (mut m Money) SameCurrency(omut m Money) bool {
// 	return m.currency.equals(om.currency)
// }

// fn (mut m Money) assertSameCurrency(omut m Money) error {
// 	if !m.SameCurrency(om) {
// 		return errors.New("currencies don't match")
// 	}

// 	return nil
// }

// fn (mut m Money) compare(omut m Money) int {
// 	switch {
// 	case m.amount.val > om.amount.val:
// 		return 1
// 	case m.amount.val < om.amount.val:
// 		return -1
// 	}

// 	return 0
// }

// // Equals checks equality between two Money types.
// fn (mut m Money) Equals(omut m Money) (bool, error) {
// 	if err := m.assertSameCurrency(om); err != nil {
// 		return false, err
// 	}

// 	return m.compare(om) == 0, nil
// }

// // GreaterThan checks whether the value of Money is greater than the other.
// fn (mut m Money) GreaterThan(omut m Money) (bool, error) {
// 	if err := m.assertSameCurrency(om); err != nil {
// 		return false, err
// 	}

// 	return m.compare(om) == 1, nil
// }

// // GreaterThanOrEqual checks whether the value of Money is greater or equal than the other.
// fn (mut m Money) GreaterThanOrEqual(omut m Money) (bool, error) {
// 	if err := m.assertSameCurrency(om); err != nil {
// 		return false, err
// 	}

// 	return m.compare(om) >= 0, nil
// }

// // LessThan checks whether the value of Money is less than the other.
// fn (mut m Money) LessThan(omut m Money) (bool, error) {
// 	if err := m.assertSameCurrency(om); err != nil {
// 		return false, err
// 	}

// 	return m.compare(om) == -1, nil
// }

// // LessThanOrEqual checks whether the value of Money is less or equal than the other.
// fn (mut m Money) LessThanOrEqual(om &Money) (bool, error) {
// 	if err := m.assertSameCurrency(om); err != nil {
// 		return false, err
// 	}

// 	return m.compare(om) <= 0, nil
// }

// // IsZero returns boolean of whether the value of Money is equals to zero.
// fn (mut m Money) IsZero() bool {
// 	return m.amount.val == 0
// }

// // IsPositive returns boolean of whether the value of Money is positive.
// fn (mut m Money) IsPositive() bool {
// 	return m.amount.val > 0
// }

// // IsNegative returns boolean of whether the value of Money is negative.
// fn (mut m Money) IsNegative() bool {
// 	return m.amount.val < 0
// }

// // Absolute returns new Money struct from given Money using absolute monetary value.
// fn (mut m Money) Absolute() &Money {
// 	return &Money{amount: mutate.calc.absolute(m.amount), currency: m.currency}
// }

// // Negative returns new Money struct from given Money using negative monetary value.
// fn (mut m Money) Negative() &Money {
// 	return &Money{amount: mutate.calc.negative(m.amount), currency: m.currency}
// }

// // Add returns new Money struct with value representing sum of Self and Other Money.
// fn (mut m Money) Add(om &Money) (&Money, error) {
// 	if err := m.assertSameCurrency(om); err != nil {
// 		return nil, err
// 	}

// 	return &Money{amount: mutate.calc.add(m.amount, om.amount), currency: m.currency}, nil
// }

// // Subtract returns new Money struct with value representing difference of Self and Other Money.
// fn (mut m Money) Subtract(omut m Money) (&Money, error) {
// 	if err := m.assertSameCurrency(om); err != nil {
// 		return nil, err
// 	}

// 	return &Money{amount: mutate.calc.subtract(m.amount, om.amount), currency: m.currency}, nil
// }

// // Multiply returns new Money struct with value representing Self multiplied value by multiplier.
// fn (mut m Money) Multiply(mul int64) &Money {
// 	return &Money{amount: mutate.calc.multiply(m.amount, mul), currency: m.currency}
// }

// // Round returns new Money struct with value rounded to nearest zero.
// fn (mut m Money) Round() &Money {
// 	return &Money{amount: mutate.calc.round(m.amount, m.currency.Fraction), currency: m.currency}
// }

// Split returns slice of Money structs with split Self value in given number.
// After division leftover pennies will be distributed round-robin amongst the parties.
// This means that parties listed first will likely receive more pennies than ones that are listed later.
fn (mut m Money) split(n int) ([]&Money, error) {
	if n <= 0 {
		return nil, errors.New("split must be higher than zero")
	}

	a := mutate.calc.divide(m.amount, int64(n))
	ms := make([]&Money, n)

	for i := 0; i < n; i++ {
		ms[i] = &Money{amount: a, currency: m.currency}
	}

	l := mutate.calc.modulus(m.amount, int64(n)).val

	// Add leftovers to the first parties.
	for p := 0; l != 0; p++ {
		ms[p].amount = mutate.calc.add(ms[p].amount, &Amount{1})
		l--
	}

	return ms, nil
}

// // Allocate returns slice of Money structs with split Self value in given ratios.
// // It lets split money by given ratios without losing pennies and as Split operations distributes
// // leftover pennies amongst the parties with round-robin principle.
// fn (mut m Money) Allocate(rs ...int) ([]&Money, error) {
// 	if len(rs) == 0 {
// 		return nil, errors.New("no ratios specified")
// 	}

// 	// Calculate sum of ratios.
// 	var sum int
// 	for _, r := range rs {
// 		sum += r
// 	}

// 	var total int64
// 	ms := make([]&Money, 0, len(rs))
// 	for _, r := range rs {
// 		party := &Money{
// 			amount:   mutate.calc.allocate(m.amount, r, sum),
// 			currency: m.currency,
// 		}

// 		ms = append(ms, party)
// 		total += party.amount.val
// 	}

// 	// Calculate leftover value and divide to first parties.
// 	lo := m.amount.val - total
// 	sub := int64(1)
// 	if lo < 0 {
// 		sub = -sub
// 	}

// 	for p := 0; lo != 0; p++ {
// 		ms[p].amount = mutate.calc.add(ms[p].amount, &Amount{sub})
// 		lo -= sub
// 	}

// 	return ms, nil
// }

// // Display lets represent Money struct as string in given Currency value.
pub fn (mut m Money) display() string {
	mut c := m.currency.get()
	return c.formatter().format(m.amount.val)
}

// // AsMajorUnits lets represent Money struct as subunits (float64) in given Currency value
// fn (mut m Money) AsMajorUnits() float64 {
// 	c := m.currency.get()
// 	return c.Formatter().ToMajorUnits(m.amount.val)
// }

// // UnmarshalJSON is implementation of json.Unmarshaller
// fn (mut m Money) UnmarshalJSON(b []byte) error {
// 	return UnmarshalJSON(m, b)
// }

// // MarshalJSON is implementation of json.Marshaller
// fn (mut m Money) MarshalJSON() ([]byte, error) {
// 	return MarshalJSON(m)

// }
