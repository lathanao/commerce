module money
 

// Currency represents money currency information required for formatting.
struct Currency {
	code        string
	numeric_code string
	fraction    int
	grapheme    string
	template    string
	decimal     string
	thousand    string
}

// currencies represents a collection of currency.
const (
		currencies = map{
		"EUR": Currency{decimal: ".", thousand: ",", code: "EUR", fraction: 2, numeric_code: "978", grapheme: "\u20ac", template: "$1"},
		"JPY": Currency{decimal: ".", thousand: ",", code: "JPY", fraction: 0, numeric_code: "392", grapheme: "\u00a5", template: "$1"},
		"THB": Currency{decimal: ".", thousand: ",", code: "THB", fraction: 2, numeric_code: "764", grapheme: "\u0e3f", template: "$1"},
		"USD": Currency{decimal: ".", thousand: ",", code: "USD", fraction: 2, numeric_code: "840", grapheme: "$", template: "$1"}
	}
)

// // AddCurrency lets you insert or update currency in currencies list.
// fn add_currency(code, Grapheme, Template, Decimal, Thousand string, Fraction int) &Currency {
// 	currencies[code] := &Currency{
// 		code:     code,
// 		grapheme: Grapheme,
// 		template: Template,
// 		decimal:  Decimal,
// 		thousand: Thousand,
// 		fraction: Fraction,
// 	}

// 	return currencies[code]
// }

fn new_currency(code string) &Currency {
	return &Currency{code: code.to_upper()}
}

// GetCurrency returns the currency given the code.
fn get_currency(code string) Currency {
	return currencies[code]
}

// Formatter returns currency formatter representing
// used currency structure.
fn (mut c Currency) formatter() &Formatter {
	return &Formatter{
		fraction: c.fraction,
		decimal:  c.decimal,
		thousand: c.thousand,
		grapheme: c.grapheme,
		template: c.template,
	}
}

// getDefault represent default currency if currency is not found in currencies list.
// Grapheme and Code fields will be changed by currency code.

// getDefault represent default currency if currency is not found in currencies list.
fn (mut c Currency) get_default() &Currency {
	return &Currency{decimal: ".", thousand: ",", code: c.code, fraction: 2, grapheme: c.code, template: "1$"}
}

// get extended currency using currencies list.
fn (mut c Currency) get() &Currency {
	// if curr, ok := currencies[c.Code]; ok {
	// 	return curr
	// }

	return c.get_default()
}

fn (mut c Currency) equals(oc Currency) bool {
	return c.code == oc.code
}