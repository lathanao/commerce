module money

import math


// Formatter stores Money formatting information.
struct Formatter {
	fraction int
	decimal  string
	thousand string
	grapheme string
	template string
}

// NewFormatter creates new Formatter instance.
fn new_formatter(fraction int, decimal string, thousand string, grapheme string, template string) &Formatter {
	return &Formatter{
		fraction: fraction,
		decimal:  decimal,
		thousand: thousand,
		grapheme: grapheme,
		template: template,
	}
}

// Format returns string of formatted integer using given currency template.
fn (mut f Formatter) format(amount int) string {

	//Work with absolute amount value
	//sa := strconv.FormatInt(f.abs(amount), 10)
	saa := int(math.abs(amount))
	mut sa := saa.str()

	println('-------- init --------')
	println(sa)
	println(sa.len)
	println(saa)


	if saa.str().len <= f.fraction {
		//sa = strings.Repeat("0", f.fraction - sa.str().len  +1 ) + sa
		
		//sa = strings.Repeat("0", f.fraction - sa.str().len  +1 ) + sa	
		sa = '0'.repeat(f.fraction - saa.str().len  +1 ) + saa.str()
	}

	println('-------- thousand --------')
	if f.thousand != "" {
		for i := sa.str().len  - f.fraction - 3; i > 0; i -= 3 {
			sa = sa[..i] + f.thousand + sa[i..]
		}
	}

	println('-------- fraction --------')
	println(sa)
	println(sa.len)
	println(sa.len-f.fraction)
	println(sa.len-f.fraction)

	if f.fraction > 0 {
		sa = sa[..sa.len-f.fraction] + f.decimal + sa[sa.len-f.fraction..]
	}

	println('-------- replace_once --------')
	sa = f.template.replace_once("1", sa)
	sa = sa.replace_once("$", f.grapheme)

	// Add minus sign for negative amount.
	if amount < 0 {
		sa = "-" + sa
	}

	// return sa
	return sa
}

// ToMajorUnits returns float64 representing the value in sub units using the currency data
fn (mut f Formatter) to_major_units(amount int) f64 {
	if f.fraction == 0 {
		return f64(amount)
	}

	return f64(amount) / f64(math.pow(f.fraction, 10))
}

// abs return absolute value of given integer.
fn (f Formatter) abs(amount int) int {
	if amount < 0 {
		return -amount
	}

	return amount
}