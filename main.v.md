module main

import rand
import sqlite
import vweb
import classes
import time
import os
import flag
import encoding.base64

const (
	port = 8081
)

struct App {
	vweb.Context
mut:
	cnt 				int
	db   				sqlite.DB
	factory 		classes.Factory
	logged_in   bool
}

struct Post {
pub mut:
	id      int
	title   string
	body    string
	deleted int
	date    int
}

fn main() {
	mut app := App{
		db: sqlite.connect(':memory:') or { panic(err) }
	}
	
	app.factory.start()
	println(app.factory)

	app.handle_static('/var/www/vproject/blog/HTMLElement', false)
	app.handle_static('/var/www/vproject/blog/static/css', false)
	// app.scan_static_directory('/var/www/vproject/blog/static/js', '/static/js')
	// app.scan_static_directory('/var/www/vproject/blog/static/img', '/img')

	// vweb.run<App>(port)
	
	vweb.run<App>(&app, 8181)
}

pub fn (mut app App) init_once() {
	


	// products_list := app.factory
	// println(products_list)
}

pub fn (mut app App) index() vweb.Result {
	if app.logged_in() {
		return $vweb.html()
	}
	else {
		return app.redirect('/login')
	}
}

['/login']
pub fn (mut app App) login() vweb.Result {
	if app.logged_in() {
		return app.redirect('/')
	}
	else {
		return $vweb.html()
	}
}

[post]
['/login']
pub fn (mut app App) login_post() vweb.Result {

	email := app.form['email']
	password := app.form['password']
	//println(email)
	//println(password)
	if email.len > 0 && password.len > 4 {
		app.set_cookie(name: 'logged', value: '1')
		return app.redirect('/')
	}
	return app.redirect('/login')
}

['/catalog/product']
pub fn (mut app App) catalog() vweb.Result {
	if app.logged_in() != true {
		return app.redirect('/login')
	}
	link_new_form := '/catalog/product/0'
	//products_list := [app.factory.product()]
	products_list := app.factory.product().filter_by_query({'page': '1'})
	//println(products_list)
	return $vweb.html()
}

pub fn (mut app App) logged_in() bool {
	islogged := app.get_cookie('logged') or { return false }
	//println(islogged)

	return islogged != ''
}

['/catalog/product/:product_id']
pub fn (mut app App) catalog_productform(product_id string) vweb.Result {
	//println('id: $product_id')
	mut product := app.factory.product().filter_by_id({'id': product_id})
	//println(product)
	link_form_save := '/catalog/product/save'
	return $vweb.html()
}


['/catalog/product/save']
pub fn (mut app App) catalog_save_productform(product_id string) vweb.Result {
	//mut product := app.factory.product().hydrate_form_post(app.form).save()
	//println(product)
	//return app.redirect('/catalog/' + product.id.str())
	
	return app.redirect('/catalog/product')
}

/**
* Returns metadata pool.
*
* @return \Magento\Framework\EntityManager\MetadataPool
* @deprecated 101.0.0
*/
[post]
['/catalog/product/save']
pub fn (mut app App) catalog_save_productform_post() vweb.Result {
	mut product := app.factory.product().hydrate_form_post(app.form).save()
	//println(product)
	//return app.redirect('/catalog/' + product.id.str())
	
	return app.redirect('/catalog/product')
}



['/catalog/product/delete']
pub fn (mut app App) catalog_product_delete() vweb.Result {
	mut product := app.factory.product().hydrate_form_post(app.form).save()
	//println(product)
	//return app.redirect('/catalog/' + product.id.str())
	
	return app.redirect('/catalog/product')
}

[post]
['/catalog/product/delete']
pub fn (mut app App) catalog_product_delete_post() vweb.Result {
	mut product := app.factory.product().hydrate_form_post(app.form).delete()
	//println(product)
	//return app.redirect('/catalog/' + product.id.str())
	
	return app.redirect('/catalog/product')
}

['/catalog/product/image/upload']
pub fn (mut app App) catalog_product_image_upload() vweb.Result {
	println( "PROCESS /catalog/product/image/upload" )
	println( app.form )
	return app.text('{"done":"Ok"}')
}

[post]
['/catalog/product/image/upload']
pub fn (mut app App) catalog_product_image_upload_post() vweb.Result {

	// println( "PROCESS /catalog/product/image/upload" )

	println( "=============== BLOG" )
	// println( app.form )
	// println( app.files )

	// mut f := os.create("pub/product/" + app.files['image'][0].filename) or { panic(err) }

	// mut data := app.files['image'][0].data
	// //mut con := base64.code(data)

	// f.write_string(data) or { panic(err) }
	// f.close()
	//println(app.files['image'][0].filename)

	return app.text('{"done":"Ok"}')
}


// ['/catalog/product/image/stream']
// pub fn (mut app App) catalog_product_image_stream() vweb.Result {

// 	app.open_event_stream("data: hello get 1\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("data: hello get 2\n\n")
// 	app.emit_event_stream("mess: hello fuck\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("done: hello get 22\n\n")
// 	time.sleep_ms(500)
// 	return app.close_event_stream()
// }

// [post]
// ['/catalog/product/image/stream']
// pub fn (mut app App) catalog_product_image_stream_post() vweb.Result {

// 	app.enable_chunked_transfer(40)

// 	app.open_event_stream("data: hello get 3\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("data: hello get 4\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("done: hello get 44\n\n")
// 	time.sleep_ms(500)
// 	return app.close_event_stream()
// }


// pub fn (mut app App) client_ip(username string) ?string {
// 	ip := app.conn.peer_ip() or { return none }
// 	return app.ses.make_password(ip, '${username}token')
// }