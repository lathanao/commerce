'use strict';

(function() {
  class EditableList extends HTMLElement {
    constructor() {
      // establish prototype chain
      super();

      // attaches shadow tree and returns shadow root reference
      // https://developer.mozilla.org/en-US/docs/Web/API/Element/attachShadow
      const shadow = this.attachShadow({ mode: 'open' });

      // creating a container for the editable-list component
      // const editableListContainer = document.createElement('div');

      const editableListContainer = document.createElement('report-card-e');
      editableListContainer.classList.add('report-card');
      //editableListContainer.classList.add('report-card');
      // creating the inner HTML of the editable list element
      editableListContainer.innerHTML = `
 
            <div class="card">
                <div class="card-body flex flex-col">
                
                    <!-- top -->
                    <div class="flex flex-row justify-between items-center">
                        <div class="h6 text-green-700 fad fa-users"></div>
                        <span class="rounded-full text-white badge bg-teal-400 text-xs">
                        150%
                        <i class="fal fa-chevron-up ml-1"></i>
                        </span>
                        </div>
                        <!-- end top -->
                        
                        <!-- bottom -->
                        <div class="mt-8">
                        <h1 class="h5 num-4">3116</h1>
                        <p>new Visitor</p>
                    </div>
                    <!-- end bottom -->
                    
                    </div>
                </div>
            <div class="footer bg-white p-1 mx-4 border border-t-0 rounded rounded-t-none"></div>
    
        <style>
            *, ::before, ::after {
                box-sizing: border-box;
                border-width: 0;
                border-style: solid;
                border-color: #e2e8f0;
            }
            .mt-8 {
                margin-top: 2rem;
            }
            .card .card-body {
                padding: 1.5rem;
            }
            .flex-col {
                flex-direction: column;
            }
            .flex {
                display: flex;
            }
            .text-red-700 {
                --text-opacity: 1;
                color: #c53030;
                color: rgba(197, 48, 48, var(--text-opacity));
            }
            .h6 {
                font-size: 1.25rem;
                font-weight: 800;
            }
            .fad {
                position: relative;
                font-family: "Font Awesome 5 Duotone";
                font-weight: 900;
            }
            .fa, .fab, .fad, .fal, .far, .fas {
                -moz-osx-font-smoothing: grayscale;
                -webkit-font-smoothing: antialiased;
                display: inline-block;
                font-style: normal;
                font-variant: normal;
                text-rendering: auto;
                line-height: 1;
            }
            .card {
                border-radius: 0.25rem;
                --bg-opacity: 1;
                background-color: #fff;
                background-color: rgba(255, 255, 255, var(--bg-opacity));
                border-width: 1px;
                --border-opacity: 1;
                border-color: #e2e8f0;
                border-color: rgba(226, 232, 240, var(--border-opacity));
            }
            body, h1, h2, h3, h4, h5, h6, p, a {
                text-transform: capitalize;
            }
            .h5 {
                font-size: 1.5rem;
                font-weight: 800;
            }
            p {
                letter-spacing: 0.05em;
                font-size: 0.875rem;
                --text-opacity: 1;
                color: #a0aec0;
                color: rgba(160, 174, 192, var(--text-opacity));
                font-family: Ubuntu, Sans-serif;
                line-height: 1.5;
            }
            .text-white {
                --text-opacity: 1;
                color: #fff;
                color: rgba(255, 255, 255, var(--text-opacity));
            }
            .text-xs {
                font-size: 0.75rem;
            }
            .rounded-full {
                border-radius: 9999px;
            }
            .bg-teal-400 {
                --bg-opacity: 1;
                background-color: #4fd1c5;
                background-color: rgba(79, 209, 197, var(--bg-opacity));
            }
            .badge {
                padding: 2px 0;
                padding-left: 0.5rem;
                padding-right: 0.5rem;
            }
        </style>`;



      // appending the container to the shadow DOM
      shadow.appendChild(editableListContainer);
    }
  }

  // let the browser know about the custom element
  customElements.define('report-card', EditableList);
})();
