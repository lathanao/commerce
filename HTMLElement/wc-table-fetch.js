export class WCTable extends HTMLElement {
  static get observedAttributes () {
    return ['src', 'no-headers']
  }

  attributeChangedCallback (name, oldValue, newValue) {
    if (!this.__initialized) { return }
    if (oldValue !== newValue) {
      this[name] = newValue
    }
  }

  get src () { return this.getAttribute('src') }
  set src (value) {
    this.setAttribute('src', value)
    this.setSrc(value)
  }

  get value () { return this.__data }
  set value (value) {
    this.setValue(value)
  }

  constructor () {
    console.log('Constructor WCTable')
    super()
    this.__initialized = false
    this.__data = []
    this.__table = document.createElement('table')
    this.appendChild(this.__table)
  }

  async connectedCallback () {
    if (this.hasAttribute('src')) {
      fetch(this.src)
      .then(res => res.text())
      .then(res => {
        this.__data = JSON.parse(res)
        this.render()
      })
    }
    this.__initialized = true
  }

  async fetchSrc (src) {
    const response = await fetch(src)
    if (response.status !== 200) throw Error(`ERR ${response.status}: ${response.statusText}`)
    return response.text()
  }

  async fetchApi (src) {
    const response = await fetch(src)
    if (response.status !== 200) throw Error(`ERR ${response.status}: ${response.statusText}`)
    return JSON.parse(response.text())
  }

  setValue (value) {
    this.__data = value
    this.render()
  }

  render () {
    const data = this.__data
    const table = document.createElement('table')
    table.classList.add('myClass');
    const headers = data[0]
    const thead = document.createElement('thead')
    const tr = document.createElement('tr')

    Object.keys(headers).forEach(key => {
      const th = document.createElement('th')
      th.innerText = key
      tr.appendChild(th)
    })

    thead.append(tr)
    table.appendChild(thead)

    const tbody = document.createElement('tbody')
    data.forEach(row => {
      const tr = document.createElement('tr')
      Object.values(row).forEach(value => {
        const td = document.createElement('td')
        td.innerText = value
        tr.appendChild(td)
      })
      tbody.appendChild(tr)
    })

    table.appendChild(tbody)
    this.removeChild(this.__table)
    this.__table = table
    this.appendChild(this.__table)
  }
}

customElements.define('wc-table-fetch', WCTable)
