class HelloWorld extends HTMLElement {
  
  constructor() {
    console.log('Constructor HelloWorld')
    super();
    this.name = 'World';
  }
  
  // component attributes
  static get observedAttributes() {
    console.log(['name']);
    return ['name'];
  }
  
  // attribute change
  attributeChangedCallback(property, oldValue, newValue) {
    console.log(['property']);
    console.log(['oldValue']);
    console.log(['newValue']);
    if (oldValue === newValue) return;
    this[ property ] = newValue;
  }
  
  // connect component
  connectedCallback() {
    
    const 
      shadow = this.attachShadow({ mode: 'closed' }),
      hwMsg = `Hello ${ this.name }`;
    
    // append shadow DOM
    shadow.append( 
      document.getElementById('wc-helloworld').content.cloneNode(true)
    );
    // console.log(document.getElementById('hello-world').content.cloneNode(true));

    // find all slots with a hw-text class
    Array.from( shadow.querySelectorAll('slot.hw-text') )
      // update first assignedNode in slot
     .forEach( n => n.assignedNodes()[0].textContent = hwMsg );
    
  }
  
}

// register component
customElements.define( 'wc-helloworld', HelloWorld );