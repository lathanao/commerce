// Copyright (c) 2019-2020 Alexander Medvednikov. All rights reserved.
// Use of this source code is governed by a GPL license that can be found in the LICENSE file.
module classes

//import crypto.sha256
//import rand
//import os
//import time
import sqlite

[heap]
struct AdminUserAbstract {
mut:
    pragma      []string [skip]
    schema      []string [skip]
    schema_lang []string [skip]
    db  		sqlite.DB [skip]
    cache		map[string]string [skip]
}

fn (mut a AdminUser) init() &AdminUser {
	//println('---AdminUser-----init-------')
	res,_ := a.db.exec( 'pragma table_info("admin_user");' )
	//println(res)
	for line in res {
		a.schema << line.vals[1]
	}
	return a
}

fn (a AdminUser) hydrate (data sqlite.Row) AdminUser {
	//println('---AdminUser---hydrate------')
    mut result := a
	for iii, name_field  in a.schema {
		$for field in AdminUser.fields {
			$if field.typ is int {
				if field.name == name_field {
					result.$(field.name) = data.vals[iii].int()
				}	
			}
			$if field.typ is string {
				if field.name == name_field {
					result.$(field.name) = data.vals[iii]
				}
			}
			$if field.typ is bool {
				if field.name == name_field {
					result.$(field.name) = data.vals[iii].bool()
				}
			}
			$if field.typ is f32 {
				if field.name == name_field {
					result.$(field.name) = data.vals[iii].f32()
				}
			}
		}
	}
    return result
}