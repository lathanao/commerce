module main


import vweb
import time
import rand
import math
import classes

import crypto.sha256
//import net

pub const (
	version = 0xBA //Magic byte, BrancA.
	private = "sdfdsfsfqwewq"
	nonce = ""
)

pub struct Session {
	vweb.Context
	id            int
	user_id       int
	user_ip       string
	timestamp     i64 
	token         string 
}




pub fn (mut ses Session) logout() vweb.Result {
	ses.set_cookie(name: 'SESSION', value: '')
	return ses.redirect('/login')
}

pub fn (mut ses Session) client_ip(username string) ?string {
	ip := ses.conn.peer_ip() or { return none }
	return make_password(ip, '${username}token')
}

pub fn (mut ses Session) check_user_blocked(user_id int) bool {
	//user := app.find_user_by_id(user_id) or { return false }
	return false
}

pub fn (mut ses Session) make_password(password string, username string) string {
	return make_password(password, username)
}

fn make_password(password string, username string) string {
	mut seed := [u32(username[0]), u32(username[1])]
	rand.seed(seed)
	salt := rand.i64().str()
	pw := '$password$salt'
	return sha256.sum(pw.bytes()).hex().str()
}

pub fn (mut ses Session) get_user_from_cookies()  {
	// id := ses.get_cookie('id') or { return none }
	// token := ses.get_cookie('token') or { return none }
	// mut user := ses.find_user_by_id(id.int()) or { return none }
	// ip := app.client_ip(id) or { return none }
	// if token != ses.find_user_token(user.id, ip) {
	// 	return none
	// }
	// user.b_avatar = user.avatar != ''
	// if !user.b_avatar {
	// 	user.avatar = user.username.bytes()[0].str()
	// }

}

fn gen_uuid_v4ish() string {
	// UUIDv4 format: 4-2-2-2-6 bytes per section
	a := rand.intn(math.max_i32 / 2).hex()
	b := rand.intn(math.max_i16).hex()
	c := rand.intn(math.max_i16).hex()
	d := rand.intn(math.max_i16).hex()
	e := rand.intn(math.max_i32 / 2).hex()
	f := rand.intn(math.max_i16).hex()
	return '${a:08}-${b:04}-${c:04}-${d:04}-${e:08}${f:04}'.replace(' ', '0')
}

fn encode_cookie() Session {
	ticknow := time.ticks()
	ses := Session{
		user_id: 1010, 
		user_ip: '127.0.0.1', 
		timestamp: ticknow  + i64(3600 * 1000), 
		token: gen_uuid_v4ish()
		}

	return ses
}

fn decode_cookie(jsoned_session string) Session {
	//session := json.decode(Session, jsoned_session) or { exit(1) }
	// if session.user_id > 0{
	// 	return session
	// }
	return Session{}
}

['/forgot_password']
pub fn (mut app App) admin_forgotpassword() vweb.Result {
	csrf := rand.string(30)
	app.set_cookie(name: 'csrf', value: csrf)
	return $vweb.html()
}