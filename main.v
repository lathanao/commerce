module main

import rand
import sqlite
import vweb
import classes
import time
import os
import flag
import encoding.base64
import json
import math
import mysql


const (
	port = 8082
)

struct App {
	vweb.Context
pub mut:
	cnt 				int
	db   				mysql.Connection
	factory 		classes.Factory
	logged_in   bool
}

// struct ApiResponse {
// 	token 			fn () gen_uuid_v4ish()
// 	content 		int
// }

fn main() {
	mut app := App{
		db: mysql.Connection{
				username: 'magento'
				password: 'magento'
				dbname: 'feca_1'
		}
	}

	app.db.connect() ?
	app.db.select_db('raw_pcw') ?

	app.factory.start()
	println(app.factory)

	app.handle_static(os.join_path(os.resource_abs_path('static/js')), true)
	app.handle_static(os.join_path(os.resource_abs_path('static/css')), true)
	app.handle_static(os.join_path(os.resource_abs_path('static/img')), true)
	app.handle_static(os.join_path(os.resource_abs_path('HTMLElement')), true)
	app.handle_static(os.join_path(os.resource_abs_path('HTMLElement/datawc')), true)
	app.handle_static(os.join_path(os.resource_abs_path('HTMLElement/vanillatool')), true)
	app.handle_static(os.join_path(os.resource_abs_path('webcomponents')), true)

	vweb.run<App>(&app, 8181)
}

pub fn (mut app App) init_once() {
	


	// products_list := app.factory
	// println(products_list)
}

pub fn (mut app App) index() vweb.Result {
	if app.logged_in() {
		return $vweb.html()
	}
	else {
		return app.redirect('/login')
	}
}

pub fn (mut app App) logged_in() bool {
	islogged := app.get_cookie('logged') or { return false }
	return islogged != ''
}

['/login']
pub fn (mut app App) login() vweb.Result {
	if app.logged_in() {
		return app.redirect('/')
	}
	else {
		return $vweb.html()
	}
}

['/webcomponent']
pub fn (mut app App) webcomponent() vweb.Result {
	return $vweb.html()
}

['/xycomponent']
pub fn (mut app App) xycomponent() vweb.Result {
	return $vweb.html()
}


['/api']
pub fn (mut app App) api() vweb.Result {
		mut p := []classes.Product
		p << classes.Product{
			id                  : 10
			sku                 : 'ABC'
			ean13               : '123ABC'
		}
		p << classes.Product{
			id                  : 11
			sku                 : 'DEF'
			ean13               : '456DEF'
		}

		return app.json_pretty(p)
}

[post]
['/api']
pub fn (mut app App) api_post() vweb.Result {
		mut p := []classes.Product
		p << classes.Product{
			id                  : 10
			sku                 : 'ABC'
			ean13               : '123ABC'
		}
		p << classes.Product{
			id                  : 11
			sku                 : 'DEF'
			ean13               : '456DEF'
		}
		return app.json(p)
}


[post]
['/login']
pub fn (mut app App) login_post() vweb.Result {

	email := app.form['email']
	password := app.form['password']
	//println(email)
	//println(password)
	if email.len > 0 && password.len > 4 {
		app.set_cookie(name: 'logged', value: '1')
		return app.redirect('/')
	}
	return app.redirect('/login')
}


['/catalog/product']
pub fn (mut app App) catalog() vweb.Result {
	println('=========== /catalog/product =============')
	if app.logged_in() != true {
		return app.redirect('/login')
	}
	link_new_form := '/catalog/product/0'


	// products_list := app.factory.product().filter_by_query({'page': '1'})
	products_list := app.factory.product().filter_by_query({'page': '1'})
	println(products_list)
	
	return $vweb.html()
}

['/catalog/product/:product_id']
pub fn (mut app App) catalog_productform(product_id string) vweb.Result {
	//println('id: $product_id')
	mut product := app.factory.product().filter_by_id({'id': product_id})
	//println(product)
	link_form_save := '/catalog/product/save'
	return $vweb.html()
}





['/catalog/product/save']
pub fn (mut app App) catalog_save_productform(product_id string) vweb.Result {
	//mut product := app.factory.product().hydrate_form_post(app.form).save()
	//println(product)
	//return app.redirect('/catalog/' + product.id.str())
	
	return app.redirect('/catalog/product')
}

/**
* Returns metadata pool.
*
* @return \Magento\Framework\EntityManager\MetadataPool
* @deprecated 101.0.0
*/
// [post]
// ['/catalog/product/save']
// pub fn (mut app App) catalog_save_productform_post() vweb.Result {
// 	mut product := app.factory.product().hydrate_form_post(app.form).save()
// 	//println(product)
// 	//return app.redirect('/catalog/' + product.id.str())
	
// 	return app.redirect('/catalog/product')
// }



// ['/catalog/product/delete']
// pub fn (mut app App) catalog_product_delete() vweb.Result {
// 	mut product := app.factory.product().hydrate_form_post(app.form).save()
// 	//println(product)
// 	//return app.redirect('/catalog/' + product.id.str())
	
// 	return app.redirect('/catalog/product')
// }

// [post]
// ['/catalog/product/delete']
// pub fn (mut app App) catalog_product_delete_post() vweb.Result {
// 	mut product := app.factory.product().hydrate_form_post(app.form).delete()
// 	//println(product)
// 	//return app.redirect('/catalog/' + product.id.str())
	
// 	return app.redirect('/catalog/product')
// }

['/catalog/product/image/upload']
pub fn (mut app App) catalog_product_image_upload() vweb.Result {
	println( "PROCESS /catalog/product/image/upload" )
	println( app.form )
	return app.text('{"done":"Ok"}')
}

[post]
['/catalog/product/image/upload']
pub fn (mut app App) catalog_product_image_upload_post() vweb.Result {

	// println( "PROCESS /catalog/product/image/upload" )

	println( "=============== BLOG" )
	// println( app.form )
	// println( app.files )

	// mut f := os.create("pub/product/" + app.files['image'][0].filename) or { panic(err) }

	// mut data := app.files['image'][0].data
	// //mut con := base64.code(data)

	// f.write_string(data) or { panic(err) }
	// f.close()
	//println(app.files['image'][0].filename)

	return app.text('{"done":"Ok"}')
}


// ['/catalog/product/image/stream']
// pub fn (mut app App) catalog_product_image_stream() vweb.Result {

// 	app.open_event_stream("data: hello get 1\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("data: hello get 2\n\n")
// 	app.emit_event_stream("mess: hello fuck\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("done: hello get 22\n\n")
// 	time.sleep_ms(500)
// 	return app.close_event_stream()
// }

// [post]
// ['/catalog/product/image/stream']
// pub fn (mut app App) catalog_product_image_stream_post() vweb.Result {

// 	app.enable_chunked_transfer(40)

// 	app.open_event_stream("data: hello get 3\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("data: hello get 4\n\n")
// 	time.sleep_ms(500)
// 	app.emit_event_stream("done: hello get 44\n\n")
// 	time.sleep_ms(500)
// 	return app.close_event_stream()
// }


// pub fn (mut app App) client_ip(username string) ?string {
// 	ip := app.conn.peer_ip() or { return none }
// 	return app.ses.make_password(ip, '${username}token')
// }