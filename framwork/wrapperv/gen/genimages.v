module main

#pkgconfig --cflags --libs MagickWand
#include "wand/MagickWand.h"

fn C.MagickWandGenesis()
fn C.NewMagickWand()
fn C.MagickSetSize()
fn C.MagickReadImage()
fn C.MagickAddNoiseImage()
fn C.MagickSetImageVirtualPixelMethod()
fn C.MagickBlurImage()
fn C.MagickNormalizeImage()
fn C.MagickWriteImage()
fn C.DestroyMagickWand()
fn C.MagickWandTerminus()




/* pub struct Genimages {
pub mut:
	width       int
	height      int
	nr_channels int
	ok          bool
	data        []voidptr
	ext         string
} */

fn init() {

}

fn main() {
	mw := C._MagickWand {}
  C.MagickWandGenesis()
  mw = C.NewMagickWand()
  C.MagickSetSize(mw,200,200)
  C.MagickReadImage(mw,"xc:")
  C.MagickAddNoiseImage(mw,'RandomNoise',1.2)
  C.MagickSetImageVirtualPixelMethod(mw,'TileVirtualPixelMethod')
  C.MagickBlurImage(mw,0,10)
  C.MagickNormalizeImage(mw)
  C.MagickWriteImage(mw, "testv.jpg")
  C.DestroyMagickWand(mw)
  C.MagickWandTerminus()
}
