DROP TABLE IF EXISTS product_liquid;
CREATE TABLE product_liquid
(
    id                  INTEGER                 DEFAULT '0' PRIMARY KEY AUTOINCREMENT,
    sku                 VARCHAR(13)    NULL     DEFAULT '',
    ean13               VARCHAR(13)    NULL     DEFAULT '',

    collections         TEXT           NULL     DEFAULT '',
    type                VARCHAR(13)    NULL     DEFAULT '',

    images              VARCHAR(13)    NULL     DEFAULT '',
    featured_image      VARCHAR(13)    NULL     DEFAULT '',
    featured_media      VARCHAR(13)    NULL     DEFAULT '',

    handle              VARCHAR(13)    NULL     DEFAULT '',
    variants            VARCHAR(13)    NULL     DEFAULT '',
    selected_variant    
    selected_or_first_available_variant
    first_available_variant
    has_only_default_variant

    options
    options_with_values
    price                       DECIMAL(10, 2) NOT NULL DEFAULT 0
    price_max                   DECIMAL(10, 2) NOT NULL DEFAULT 0
    price_min                   DECIMAL(10, 2) NOT NULL DEFAULT 0
    price_varies                DECIMAL(10, 2) NOT NULL DEFAULT 0
    compare_at_price_max        DECIMAL(10, 2) NOT NULL DEFAULT 0
    compare_at_price_min        DECIMAL(10, 2) NOT NULL DEFAULT 0
    compare_at_price_varies     DECIMAL(10, 2) NOT NULL DEFAULT 0

    vendor                      INTEGER        NOT NULL DEFAULT 0,
    template_suffix             VARCHAR(13)    NULL     DEFAULT '',
);

DROP TABLE IF EXISTS product_lang;
CREATE TABLE product_lang
(
    id                INTEGER      DEFAULT '0' PRIMARY KEY AUTOINCREMENT,
    id_product        INTEGER      NOT NULL DEFAULT 0,
    id_lang           INTEGER      NOT NULL DEFAULT 0,
    title             VARCHAR(128) NOT NULL DEFAULT '',
    description       TEXT         NULL     DEFAULT '',
    content           TEXT         NULL     DEFAULT '',
    link_rewrite      VARCHAR(128) NOT NULL DEFAULT '',
    meta_description  VARCHAR(255) NULL     DEFAULT '',
    meta_keywords     VARCHAR(255) NULL     DEFAULT '',
    meta_title        VARCHAR(128) NULL     DEFAULT '',

    url               VARCHAR(128) NULL     DEFAULT '',
    tags              VARCHAR(128) NULL     DEFAULT '',
    available         TINYINT(1)   NOT NULL DEFAULT 0
    published_at      DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0',
    created_at        DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0',
    modified_at       DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0'
);

DROP TABLE IF EXISTS user;
CREATE TABLE user
(
    id                  INTEGER     PRIMARY KEY AUTOINCREMENT,
    last_name           VARCHAR(32)  NOT NULL DEFAULT '',
    name                VARCHAR(32)  NOT NULL DEFAULT '',
    first_name          VARCHAR(32)  NOT NULL DEFAULT '',
    email               VARCHAR(32)  NOT NULL DEFAULT '',
    accepts_marketing   TINYINT(1)   NOT NULL DEFAULT 0,
    addresses           INTEGER      NOT NULL DEFAULT 0,
    addresses_count     TINYINT(2)   NOT NULL DEFAULT 0,
    default_address     TINYINT(2)   NOT NULL DEFAULT 0,
    has_account         TINYINT(2)   NOT NULL DEFAULT 0,
    last_order          TINYINT(2)   NOT NULL DEFAULT 0,
    orders              INTEGER      NOT NULL DEFAULT 0,
    orders_count        INTEGER      NOT NULL DEFAULT 0,
    phone               INTEGER      NOT NULL DEFAULT 0,
    tags                INTEGER      NOT NULL DEFAULT 0,
    total_spent         INTEGER      NOT NULL DEFAULT 0,
    published_at        DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0',
    created_at          DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_at         DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0'
);

DROP TABLE IF EXISTS admin_user;
CREATE TABLE admin_user
(
    id                  INTEGER               DEFAULT '0' PRIMARY KEY AUTOINCREMENT,
    last_name           VARCHAR(32)  NOT NULL DEFAULT '',
    name                VARCHAR(32)  NOT NULL DEFAULT '',
    first_name          VARCHAR(32)  NOT NULL DEFAULT '',
    email               VARCHAR(32)  NOT NULL DEFAULT '',
    password            VARCHAR(32)  NOT NULL DEFAULT '',
    accepts_marketing   TINYINT(1)   NOT NULL DEFAULT 0,
    addresses           INTEGER      NOT NULL DEFAULT 0,
    addresses_count     TINYINT(2)   NOT NULL DEFAULT 0,
    default_address     TINYINT(2)   NOT NULL DEFAULT 0,
    has_account         TINYINT(2)   NOT NULL DEFAULT 0,
    last_order          TINYINT(2)   NOT NULL DEFAULT 0,
    phone               INTEGER      NOT NULL DEFAULT 0,
    tags                INTEGER      NOT NULL DEFAULT 0,
    published_at        DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0',
    created_at          DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_at         DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0'
);
INSERT INTO admin_user (last_name, first_name, email, password) 
VALUES ('admin', 'Admin', 'admin@gg.com', '1234')


DROP TABLE IF EXISTS session;
CREATE TABLE session
(
    id                  INTEGER PRIMARY KEY AUTOINCREMENT,
    token               STRING       NOT NULL DEFAULT '',
    user_id             INTEGER      NOT NULL DEFAULT 0,
    user_ip             INTEGER      NOT NULL DEFAULT '',
    created_at          DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_at         DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:0'
);
INSERT INTO session (token, id, ip) VALUES ('00ac8825-63bd-4c42-3d3c-30a587562011', 111, '127.0.0.1')



