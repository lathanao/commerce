## sdasd


product.id
product.title
product.description
product.content
product.collections
product.type

product.images
product.featured_image
product.featured_media

product.handle
product.variants
product.selected_variant
product.selected_or_first_available_variant
product.first_available_variant
product.has_only_default_variant

product.options
product.options_with_values
product.price
product.price_max
product.price_min
product.price_varies
product.compare_at_price_max
product.compare_at_price_min
product.compare_at_price_varies

product.url
product.tags
product.vendor
product.template_suffix

product.available
product.published_at
product.created_at
product.modified_at



shop.address
shop.collections_count
shop.currency
shop.description
shop.domain
shop.email
shop.enabled_currencies
shop.enabled_locales
shop.enabled_payment_types
shop.metafields
shop.money_format
shop.money_with_currency_format
shop.name
shop.password_message
shop.permanent_domain
shop.phone
shop.policies
shop.privacy_policy
shop.refund_policy
shop.shipping_policy
shop.terms_of_service
shop.products_count
shop.taxes-included
shop.types
shop.url
shop.secure_url
shop.vendors