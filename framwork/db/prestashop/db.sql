CREATE TABLE lang
(
    id_lang  INTEGER PRIMARY KEY AUTOINCREMENT,
    name     VARCHAR(32) NOT NULL,
    active   TINYINT     NOT NULL DEFAULT 0,
    iso_code CHAR(2)     NOT NULL
);

CREATE TABLE image_type
(
    id_image_type INTEGER PRIMARY KEY AUTOINCREMENT,
    name          VARCHAR(16) NOT NULL,
    width         INTEGER     NOT NULL,
    height        INTEGER     NOT NULL,
    products      BOOL        NOT NULL DEFAULT 1,
    categories    BOOL        NOT NULL DEFAULT 1,
    manufacturers BOOL        NOT NULL DEFAULT 1,
    suppliers     BOOL        NOT NULL DEFAULT 1
);

CREATE TABLE module
(
    id_module INTEGER PRIMARY KEY AUTOINCREMENT,
    name      VARCHAR(64) NOT NULL,
    active    INTEGER     NOT NULL DEFAULT 0
);

CREATE TABLE manufacturer
(
    id_manufacturer INTEGER PRIMARY KEY AUTOINCREMENT,
    name            VARCHAR(64) NOT NULL,
    date_add        DATETIME    NOT NULL,
    date_upd        DATETIME    NOT NULL
);

CREATE TABLE manufacturer_lang
(
    id_manufacturer INTEGER NOT NULL,
    id_lang         INTEGER NOT NULL,
    description     TEXT    NULL
);

CREATE TABLE hook
(
    id_hook     INTEGER PRIMARY KEY AUTOINCREMENT,
    name        VARCHAR(64) NOT NULL,
    title       VARCHAR(64) NOT NULL,
    description TEXT        NULL,
    position    BOOL        NOT NULL DEFAULT 1
);

CREATE TABLE discount_type
(
    id_discount_type INTEGER PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE currency
(
    id_currency     INTEGER PRIMARY KEY AUTOINCREMENT,
    name            VARCHAR(32)    NOT NULL,
    iso_code        VARCHAR(3)     NOT NULL DEFAULT 0,
    sign            VARCHAR(8)     NOT NULL,
    format          TINYINT(1)     NOT NULL DEFAULT 0,
    conversion_rate DECIMAL(13, 6) NOT NULL,
    deleted         TINYINT(1)     NOT NULL DEFAULT 0
);

CREATE TABLE gender
(
    id_gender INTEGER PRIMARY KEY AUTOINCREMENT,
    iso_code  TINYINT(3)  NOT NULL,
    name      VARCHAR(16) NOT NULL
);

CREATE TABLE feature
(
    id_feature INTEGER PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE order_state
(
    id_order_state INTEGER PRIMARY KEY AUTOINCREMENT,
    invoice        TINYINT(1)  NULL     DEFAULT 0,
    send_email     TINYINT(1)  NOT NULL DEFAULT 0,
    color          VARCHAR(32) NULL,
    unremovable    TINYINT(1)  NOT NULL,
    logable        TINYINT(1)  NOT NULL DEFAULT 0
);

CREATE TABLE tab
(
    id_tab     INTEGER PRIMARY KEY AUTOINCREMENT,
    id_parent  INTEGER     NOT NULL,
    class_name VARCHAR(64) NOT NULL,
    position   INTEGER     NOT NULL
);

CREATE TABLE supplier
(
    id_supplier INTEGER PRIMARY KEY AUTOINCREMENT,
    name        VARCHAR(64) NOT NULL,
    date_add    DATETIME    NOT NULL,
    date_upd    DATETIME    NOT NULL
);

CREATE TABLE supplier_lang
(
    id_supplier INTEGER NOT NULL,
    id_lang     INTEGER NOT NULL,
    description TEXT    NULL
);

CREATE TABLE zone
(
    id_zone INTEGER PRIMARY KEY AUTOINCREMENT,
    name    VARCHAR(64) NOT NULL,
    active  TINYINT(1)  NOT NULL DEFAULT 0,
    enabled TINYINT(1)  NOT NULL DEFAULT 0
);

CREATE TABLE tax
(
    id_tax INTEGER PRIMARY KEY AUTOINCREMENT,
    rate   FLOAT NOT NULL
);

CREATE TABLE search
(
    id_search INTEGER     NOT NULL,
    keyword   VARCHAR(64) NOT NULL,
    results   INTEGER     NOT NULL DEFAULT 0,
    date      DATETIME    NOT NULL
);

CREATE TABLE quick_access
(
    id_quick_access INTEGER PRIMARY KEY AUTOINCREMENT,
    new_window      TINYINT(1)   NOT NULL DEFAULT 0,
    link            VARCHAR(128) NOT NULL
);

CREATE TABLE profile
(
    id_profile INTEGER PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE range_weight
(
    id_range_weight INTEGER PRIMARY KEY AUTOINCREMENT,
    id_carrier      INTEGER                 DEFAULT NULL,
    delimiter1      decimal(13, 6) NOT NULL DEFAULT 0.000000,
    delimiter2      decimal(13, 6) NOT NULL DEFAULT 0.000000
);

CREATE TABLE range_price
(
    id_range_price INTEGER PRIMARY KEY AUTOINCREMENT,
    id_carrier     INTEGER        DEFAULT NULL,
    delimiter1     FLOAT NOT NULL DEFAULT 0,
    delimiter2     FLOAT NOT NULL DEFAULT 0
);

CREATE TABLE category
(
    id_category INTEGER PRIMARY KEY AUTOINCREMENT,
    id_parent   INTEGER    NOT NULL,
    level_depth TINYINT(3) NOT NULL DEFAULT 0,
    active      TINYINT(1) NOT NULL DEFAULT 0,
    date_add    DATETIME   NOT NULL,
    date_upd    DATETIME   NOT NULL
);

CREATE TABLE configuration
(
    id_configuration INTEGER PRIMARY KEY AUTOINCREMENT,
    name             VARCHAR(32) NOT NULL,
    value            TEXT        NULL,
    date_add         DATETIME    NOT NULL,
    date_upd         DATETIME    NOT NULL
);

CREATE TABLE accessory
(
    id_product_1 INTEGER NOT NULL,
    id_product_2 INTEGER NOT NULL
);

CREATE TABLE attribute_group
(
    id_attribute_group INTEGER PRIMARY KEY AUTOINCREMENT,
    is_color_group     TINYINT(1) NOT NULL DEFAULT 0
);

CREATE TABLE carrier
(
    id_carrier        INTEGER PRIMARY KEY AUTOINCREMENT,
    id_tax            INT(10)      NULL     DEFAULT 0,
    name              VARCHAR(64)  NOT NULL,
    url               VARCHAR(255) NULL,
    active            TINYINT(1)   NOT NULL DEFAULT 0,
    deleted           TINYINT      NOT NULL DEFAULT 0,
    shipping_handling TINYINT(1)   NOT NULL DEFAULT 1,
    range_behavior    TINYINT(1)   NOT NULL DEFAULT 0
);

CREATE TABLE contact
(
    id_contact INTEGER PRIMARY KEY AUTOINCREMENT,
    email      VARCHAR(128) NOT NULL,
    position   TINYINT(2)   NOT NULL default '0'
);

CREATE TABLE customer
(
    id_customer INTEGER PRIMARY KEY AUTOINCREMENT,
    id_gender   INTEGER      NOT NULL,
    secure_key  VARCHAR(32)  NOT NULL DEFAULT '-1',
    email       VARCHAR(128) NOT NULL,
    passwd      VARCHAR(32)  NOT NULL,
    birthday    DATE         NULL,
    lastname    VARCHAR(32)  NOT NULL,
    newsletter  TINYINT(1)   NOT NULL DEFAULT 0,
    optin       TINYINT(1)   NOT NULL DEFAULT 0,
    firstname   VARCHAR(32)  NOT NULL,
    active      TINYINT(1)   NOT NULL DEFAULT 0,
    date_add    DATETIME     NOT NULL,
    date_upd    DATETIME     NOT NULL
);

CREATE TABLE country
(
    id_country      INTEGER PRIMARY KEY AUTOINCREMENT,
    id_zone         INTEGER    NOT NULL,
    iso_code        VARCHAR(3) NOT NULL,
    active          TINYINT(1) NOT NULL DEFAULT 0,
    contains_states TINYINT(1) NOT NULL DEFAULT 0,
    deleted         TINYINT(1) NOT NULL DEFAULT 0
);

CREATE TABLE feature_value
(
    id_feature_value INTEGER PRIMARY KEY AUTOINCREMENT,
    id_feature       INTEGER NOT NULL,
    custom           TINYINT NULL
);

CREATE TABLE tag
(
    id_tag  INTEGER PRIMARY KEY AUTOINCREMENT,
    id_lang INTEGER     NOT NULL,
    name    VARCHAR(32) NOT NULL
);

CREATE TABLE employee
(
    id_employee INTEGER PRIMARY KEY AUTOINCREMENT,
    id_profile  INTEGER      NOT NULL,
    lastname    VARCHAR(32)  NOT NULL,
    firstname   VARCHAR(32)  NOT NULL,
    email       VARCHAR(128) NOT NULL,
    passwd      VARCHAR(32)  NOT NULL,
    active      TINYINT(1)   NOT NULL DEFAULT 0
);

CREATE TABLE connections
(
    id_connections INTEGER PRIMARY KEY AUTOINCREMENT,
    id_customer    INTEGER     NOT NULL,
    ip_address     VARCHAR(16) NOT NULL
);

CREATE TABLE attribute
(
    id_attribute       INTEGER PRIMARY KEY AUTOINCREMENT,
    id_attribute_group INTEGER     NOT NULL,
    color              VARCHAR(32) NULL DEFAULT NULL
);

CREATE TABLE tab_lang
(
    id_lang INTEGER     NOT NULL,
    id_tab  INTEGER     NOT NULL,
    name    VARCHAR(32) NULL
);

CREATE TABLE address
(
    id_address      INTEGER PRIMARY KEY AUTOINCREMENT,
    id_country      INTEGER      NOT NULL,
    id_state        INTEGER      NULL,
    id_customer     INTEGER      NOT NULL DEFAULT 0,
    id_manufacturer INT(10)      NOT NULL DEFAULT 0,
    id_supplier     INT(10)      NOT NULL DEFAULT 0,
    alias           VARCHAR(32)  NOT NULL,
    company         VARCHAR(32)  NULL,
    lastname        VARCHAR(32)  NOT NULL,
    firstname       VARCHAR(32)  NOT NULL,
    address1        VARCHAR(128) NOT NULL,
    address2        VARCHAR(128) NULL,
    postcode        VARCHAR(12)  NULL,
    city            VARCHAR(64)  NOT NULL,
    other           TEXT         NULL,
    phone           VARCHAR(16)  NULL,
    phone_mobile    VARCHAR(16)  NULL,
    date_add        DATETIME     NOT NULL,
    date_upd        DATETIME     NOT NULL,
    active          TINYINT(1)   NOT NULL DEFAULT 1,
    deleted         TINYINT(1)   NOT NULL DEFAULT 0
);

CREATE TABLE order_state_lang
(
    id_order_state INTEGER     NOT NULL,
    id_lang        INTEGER     NOT NULL,
    name           VARCHAR(64) NOT NULL,
    template       VARCHAR(64) NOT NULL
);

CREATE TABLE attribute_group_lang
(
    id_attribute_group INTEGER      NOT NULL,
    id_lang            INTEGER      NOT NULL,
    name               VARCHAR(128) NOT NULL,
    public_name        VARCHAR(64)  NOT NULL
);

CREATE TABLE attribute_lang
(
    id_attribute INTEGER      NOT NULL,
    id_lang      INTEGER      NOT NULL,
    name         VARCHAR(128) NOT NULL
);

CREATE TABLE quick_access_lang
(
    id_quick_access INTEGER     NOT NULL,
    id_lang         INTEGER     NOT NULL,
    name            VARCHAR(32) NOT NULL
);

CREATE TABLE profile_lang
(
    id_lang    INTEGER      NOT NULL,
    id_profile INTEGER      NOT NULL,
    name       VARCHAR(128) NOT NULL
);

CREATE TABLE contact_lang
(
    id_contact  INTEGER     NOT NULL,
    id_lang     INTEGER     NOT NULL,
    name        VARCHAR(32) NOT NULL,
    description TEXT        NULL
);

CREATE TABLE carrier_lang
(
    id_carrier INTEGER      NOT NULL,
    id_lang    INTEGER      NOT NULL,
    delay      VARCHAR(128) NULL
);

CREATE TABLE feature_value_lang
(
    id_feature_value INTEGER      NOT NULL,
    id_lang          INTEGER      NOT NULL,
    value            VARCHAR(255) NULL
);

CREATE TABLE feature_lang
(
    id_feature INTEGER      NOT NULL,
    id_lang    INTEGER      NOT NULL,
    name       VARCHAR(128) NULL
);

CREATE TABLE hook_module
(
    id_module INTEGER    NOT NULL,
    id_hook   INTEGER    NOT NULL,
    position  TINYINT(2) NOT NULL
);

CREATE TABLE configuration_lang
(
    id_configuration INTEGER  NOT NULL,
    id_lang          INTEGER  NOT NULL,
    value            TEXT     NULL,
    date_upd         DATETIME NULL
);

CREATE TABLE discount
(
    id_discount         INTEGER PRIMARY KEY AUTOINCREMENT,
    id_discount_type    INTEGER        NOT NULL,
    id_customer         INTEGER        NOT NULL,
    name                VARCHAR(32)    NOT NULL,
    value               DECIMAL(10, 2) NOT NULL DEFAULT 0,
    quantity            INTEGER        NOT NULL DEFAULT 0,
    quantity_per_user   INT(10)        NOT NULL DEFAULT 1,
    cumulable           TINYINT(1)     NOT NULL DEFAULT 0,
    cumulable_reduction TINYINT(1)     NOT NULL DEFAULT 0,
    date_from           DATETIME       NOT NULL,
    date_to             DATETIME       NOT NULL,
    minimal             DECIMAL(10, 2) NULL,
    active              TINYINT(1)     NOT NULL DEFAULT 0
);

CREATE TABLE access
(
    id_access  INTEGER PRIMARY KEY AUTOINCREMENT,
    id_profile INTEGER NOT NULL,
    id_tab     INTEGER NOT NULL,
    view       INTEGER NOT NULL,
    `add`      INTEGER NOT NULL,
    edit       INTEGER NOT NULL,
    `delete`   INTEGER NOT NULL
);

CREATE TABLE discount_type_lang
(
    id_discount_type INTEGER     NOT NULL,
    id_lang          INTEGER     NOT NULL,
    name             VARCHAR(64) NOT NULL
);

CREATE TABLE discount_lang
(
    id_discount INTEGER NOT NULL,
    id_lang     INTEGER NOT NULL,
    description TEXT    NULL
);

CREATE TABLE country_lang
(
    id_country INTEGER     NOT NULL,
    id_lang    INTEGER     NOT NULL,
    name       VARCHAR(64) NOT NULL
);

CREATE TABLE tax_lang
(
    id_tax  INTEGER     NOT NULL,
    id_lang INTEGER     NOT NULL,
    name    VARCHAR(32) NOT NULL
);

CREATE TABLE hook_module_exceptions
(
    id_hook_module_exceptions INTEGER PRIMARY KEY AUTOINCREMENT,
    id_module                 INTEGER      NOT NULL,
    id_hook                   INTEGER      NOT NULL,
    file_name                 VARCHAR(255) NULL
);

CREATE TABLE category_lang
(
    id_category      INTEGER      NOT NULL,
    id_lang          INTEGER      NOT NULL,
    name             VARCHAR(128) NOT NULL,
    description      TEXT         NULL,
    link_rewrite     VARCHAR(128) NOT NULL,
    meta_title       VARCHAR(128) NULL,
    meta_keywords    VARCHAR(128) NULL,
    meta_description VARCHAR(128) NULL
);

CREATE TABLE product
(
    id_product          INTEGER PRIMARY KEY AUTOINCREMENT,
    id_supplier         INTEGER        NULL,
    id_manufacturer     INTEGER        NULL,
    id_tax              INTEGER        NOT NULL,
    id_category_default INTEGER                 DEFAULT NULL,
    id_color_default    INTEGER                 DEFAULT NULL,
    on_sale             TINYINT(1)     NOT NULL DEFAULT 0,
    ean13               VARCHAR(13)    NULL,
    ecotax              DECIMAL(10, 2) NOT NULL DEFAULT 0,
    quantity            INTEGER        NOT NULL DEFAULT 0,
    price               DECIMAL(13, 6) NOT NULL DEFAULT 0.000000,
    wholesale_price     decimal(13, 6) NOT NULL DEFAULT 0.000000,
    reduction_price     DECIMAL(10, 2) NULL,
    reduction_percent   FLOAT          NULL,
    reduction_from      date                    DEFAULT NULL,
    reduction_to        date                    DEFAULT NULL,
    reference           VARCHAR(32)    NULL,
    supplier_reference  VARCHAR(32)    NULL,
    weight              FLOAT          NOT NULL DEFAULT 0,
    out_of_stock        INTEGER        NOT NULL DEFAULT 2,
    quantity_discount   TINYINT(1)     NULL     DEFAULT 0,
    active              TINYINT(1)     NOT NULL DEFAULT 0,
    date_add            DATETIME       NOT NULL,
    date_upd            DATETIME       NOT NULL
);

CREATE TABLE `product_sale`
(
    `id_product` INT(10) NOT NULL,
    `quantity`   INT(10) NOT NULL DEFAULT '0',
    `sale_nbr`   INT(10) NOT NULL DEFAULT '0',
    `date_upd`   DATE    NOT NULL
);

CREATE TABLE delivery
(
    id_delivery     INTEGER PRIMARY KEY AUTOINCREMENT,
    id_carrier      INTEGER        NOT NULL,
    id_range_price  INTEGER        NULL,
    id_range_weight INTEGER        NULL,
    id_zone         INTEGER        NOT NULL,
    price           DECIMAL(10, 2) NOT NULL
);

CREATE TABLE cart
(
    id_cart             INTEGER PRIMARY KEY AUTOINCREMENT,
    id_carrier          INTEGER    NOT NULL,
    id_lang             INTEGER    NOT NULL,
    id_address_delivery INTEGER    NOT NULL,
    id_address_invoice  INTEGER    NOT NULL,
    id_currency         INTEGER    NOT NULL,
    id_customer         INTEGER    NOT NULL,
    recyclable          TINYINT(1) NOT NULL DEFAULT 1,
    gift                TINYINT(1) NOT NULL DEFAULT 0,
    gift_message        TEXT       NULL,
    date_add            DATETIME   NOT NULL,
    date_upd            DATETIME   NOT NULL
);

CREATE TABLE orders
(
    id_order            INTEGER PRIMARY KEY AUTOINCREMENT,
    id_carrier          INTEGER        NOT NULL,
    id_lang             INTEGER        NOT NULL,
    id_customer         INTEGER        NOT NULL,
    id_cart             INTEGER        NOT NULL,
    id_currency         INTEGER        NOT NULL,
    id_address_delivery INTEGER        NOT NULL,
    id_address_invoice  INTEGER        NOT NULL,
    secure_key          VARCHAR(32)    NOT NULL DEFAULT '-1',
    payment             VARCHAR(64)    NOT NULL,
    module              VARCHAR(32)    NULL,
    recyclable          TINYINT(1)     NOT NULL DEFAULT 0,
    gift                TINYINT(1)     NOT NULL DEFAULT 0,
    gift_message        TEXT           NULL,
    shipping_number     VARCHAR(32)    NULL,
    total_discounts     DECIMAL(10, 2) NOT NULL DEFAULT 0,
    total_paid          DECIMAL(10, 2) NOT NULL DEFAULT 0,
    total_paid_real     DECIMAL(10, 2) NOT NULL DEFAULT 0,
    total_products      DECIMAL(10, 2) NOT NULL DEFAULT 0,
    total_shipping      DECIMAL(10, 2) NOT NULL DEFAULT 0,
    total_wrapping      DECIMAL(10, 2) NOT NULL DEFAULT 0,
    date_add            DATETIME       NOT NULL,
    date_upd            DATETIME       NOT NULL
);

CREATE TABLE image
(
    id_image   INTEGER PRIMARY KEY AUTOINCREMENT,
    id_product INTEGER    NOT NULL,
    position   TINYINT(2) NOT NULL DEFAULT 0,
    cover      TINYINT(1) NOT NULL DEFAULT 0
);

CREATE TABLE order_discount
(
    id_order_discount INTEGER PRIMARY KEY AUTOINCREMENT,
    id_order          INTEGER        NOT NULL,
    id_discount       INTEGER(10)    NOT NULL,
    name              VARCHAR(32)    NOT NULL,
    value             DECIMAL(10, 2) NOT NULL DEFAULT 0
);

CREATE TABLE order_detail
(
    id_order_detail            INTEGER PRIMARY KEY AUTOINCREMENT,
    id_order                   INTEGER        NOT NULL,
    product_id                 INTEGER        NOT NULL,
    product_attribute_id       INTEGER        NULL,
    product_name               VARCHAR(128)   NOT NULL,
    product_quantity           INTEGER        NOT NULL DEFAULT 0,
    product_quantity_return    INTEGER        NOT NULL DEFAULT 0,
    product_price              DECIMAL(13, 6) NOT NULL DEFAULT 0,
    product_quantity_discount  DECIMAL(13, 6) NOT NULL DEFAULT 0,
    product_ean13              VARCHAR(13)             default NULL,
    product_reference          VARCHAR(32)    NULL,
    product_supplier_reference VARCHAR(32)    NULL,
    product_weight             FLOAT          NOT NULL,
    tax_name                   VARCHAR(16)    NOT NULL,
    tax_rate                   DECIMAL(10, 2) NOT NULL DEFAULT 0,
    ecotax                     DECIMAL(10, 2) NOT NULL DEFAULT 0,
    download_hash              VARCHAR(255)            DEFAULT NULL,
    download_nb                INT(10)                 DEFAULT 0,
    download_deadline          DATETIME       NULL     DEFAULT 0
);

CREATE TABLE order_return
(
    id_order_return INTEGER PRIMARY KEY AUTOINCREMENT,
    id_customer     INTEGER    NOT NULL,
    id_order        INTEGER    NOT NULL,
    state           TINYINT(1) NOT NULL DEFAULT 1,
    question        TEXT       NOT NULL,
    date_add        DATETIME   NOT NULL,
    date_upd        DATETIME   NOT NULL
);

CREATE TABLE order_return_detail
(
    id_order_return  INTEGER NOT NULL,
    id_order_detail  INTEGER NOT NULL,
    product_quantity INT(10) NOT NULL DEFAULT 0
);

CREATE TABLE order_slip
(
    id_order_slip INTEGER PRIMARY KEY AUTOINCREMENT,
    id_customer   INTEGER  NOT NULL,
    id_order      INTEGER  NOT NULL,
    date_add      DATETIME NOT NULL,
    date_upd      DATETIME NOT NULL
);

CREATE TABLE order_slip_detail
(
    id_order_slip    INTEGER NOT NULL,
    id_order_detail  INTEGER NOT NULL,
    product_quantity INT(10) NOT NULL DEFAULT 0
);

CREATE TABLE product_attribute
(
    id_product_attribute INTEGER PRIMARY KEY AUTOINCREMENT,
    id_image             INTEGER        NULL,
    id_product           INTEGER        NOT NULL,
    reference            VARCHAR(32)    NULL,
    supplier_reference   VARCHAR(32)    NULL,
    ean13                VARCHAR(13)    NULL,
    price                DECIMAL(10, 2) NOT NULL DEFAULT 0,
    ecotax               DECIMAL(10, 2) NOT NULL DEFAULT 0,
    quantity             INTEGER        NOT NULL DEFAULT 0,
    weight               FLOAT          NOT NULL DEFAULT 0,
    default_on           TINYINT(1)     NOT NULL DEFAULT 0
);

CREATE TABLE product_lang
(
    id_product        INTEGER      NOT NULL,
    id_lang           INTEGER      NOT NULL,
    description       TEXT         NULL,
    description_short TEXT         NULL,
    link_rewrite      VARCHAR(128) NOT NULL,
    meta_description  VARCHAR(255) NULL,
    meta_keywords     VARCHAR(255) NULL,
    meta_title        VARCHAR(128) NULL,
    name              VARCHAR(128) NOT NULL,
    availability      VARCHAR(255) NULL
);

CREATE TABLE product_tag
(
    id_product INTEGER NOT NULL,
    id_tag     INTEGER NOT NULL
);

CREATE TABLE category_product
(
    id_category INTEGER NOT NULL,
    id_product  INTEGER NOT NULL,
    position    INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE cart_discount
(
    id_cart     INTEGER NOT NULL,
    id_discount INTEGER NOT NULL
);

CREATE TABLE image_lang
(
    id_image INTEGER     NOT NULL,
    id_lang  INTEGER     NOT NULL,
    legend   VARCHAR(64) NULL
);

CREATE TABLE order_history
(
    id_order_history INTEGER PRIMARY KEY AUTOINCREMENT,
    id_employee      INTEGER  NOT NULL,
    id_order         INTEGER  NOT NULL,
    id_order_state   INTEGER  NOT NULL,
    date_add         DATETIME NOT NULL
);

CREATE TABLE feature_product
(
    id_feature       INTEGER NOT NULL,
    id_product       INTEGER NOT NULL,
    id_feature_value INTEGER NOT NULL
);

CREATE TABLE cart_product
(
    id_cart              INTEGER NOT NULL,
    id_product           INTEGER NOT NULL,
    id_product_attribute INTEGER NULL,
    quantity             INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE message
(
    id_message  INTEGER PRIMARY KEY AUTOINCREMENT,
    id_cart     INTEGER    NULL,
    id_customer INTEGER    NOT NULL,
    id_employee INTEGER    NULL,
    id_order    INTEGER    NOT NULL,
    message     TEXT       NOT NULL,
    private     TINYINT(1) NOT NULL DEFAULT 1,
    date_add    DATETIME   NOT NULL
);

CREATE TABLE product_attribute_combination
(
    id_attribute         INTEGER NOT NULL,
    id_product_attribute INTEGER NOT NULL
);

CREATE TABLE `product_download`
(
    id_product_download INTEGER PRIMARY KEY AUTOINCREMENT,
    id_product          INT(10)    NOT NULL,
    display_filename    VARCHAR(255)        DEFAULT NULL,
    physically_filename VARCHAR(255)        DEFAULT NULL,
    date_deposit        DATETIME   NOT NULL,
    date_expiration     DATETIME            DEFAULT NULL,
    nb_days_accessible  INT(10)             DEFAULT NULL,
    nb_downloadable     INT(10)             DEFAULT 1,
    active              TINYINT(1) NOT NULL DEFAULT 1
);

CREATE TABLE `carrier_zone`
(
    id_carrier INT(10) NOT NULL,
    id_zone    INT(10) NOT NULL
);

CREATE TABLE `tax_zone`
(
    id_tax  INT(10) NOT NULL,
    id_zone INT(10) NOT NULL
);

CREATE TABLE `tax_state`
(
    id_tax   INT(10) NOT NULL,
    id_state INT(10) NOT NULL
);

CREATE TABLE `alias`
(
    id_alias INTEGER PRIMARY KEY AUTOINCREMENT,
    alias    VARCHAR(255) NOT NULL,
    search   VARCHAR(255) NOT NULL,
    active   TINYINT(1)   NOT NULL default 1
);

CREATE TABLE `discount_quantity`
(
    id_discount_quantity INTEGER PRIMARY KEY AUTOINCREMENT,
    id_discount_type     INT            NOT NULL,
    id_product           INT            NOT NULL,
    id_product_attribute INT            NULL,
    quantity             INT            NOT NULL,
    value                DECIMAL(10, 2) NOT NULL
);

CREATE TABLE `attribute_impact`
(
    id_attribute_impact INTEGER PRIMARY KEY AUTOINCREMENT,
    id_product          INT(11)        NOT NULL,
    id_attribute        INT(11)        NOT NULL,
    weight              float          NOT NULL,
    price               decimal(10, 2) NOT NULL
);

CREATE TABLE `state`
(
    id_state     INTEGER PRIMARY KEY AUTOINCREMENT,
    id_country   INT(11)     NOT NULL,
    id_zone      INT(11)     NOT NULL,
    name         VARCHAR(64) NOT NULL,
    iso_code     VARCHAR(3)  NOT NULL,
    tax_behavior SMALLINT(1) NOT NULL DEFAULT 0,
    active       TINYINT(1)  NOT NULL default 0
);

CREATE TABLE `order_return_state`
(
    id_order_return_state INTEGER PRIMARY KEY AUTOINCREMENT,
    color                 VARCHAR(32) default NULL
);

CREATE TABLE `order_return_state_lang`
(
    id_order_return_state INT(10)     NOT NULL,
    id_lang               INT(10)     NOT NULL,
    name                  VARCHAR(64) NOT NULL
) 