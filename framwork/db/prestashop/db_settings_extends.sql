SET NAMES 'utf8';

INSERT INTO `configuration` (`name`, `value`, `date_add`, `date_upd`) VALUES
('PS_CARRIER_DEFAULT', '2', date('now'), date('now')),
('PAYPAL_BUSINESS', 'your-paypal@address.com', date('now'), date('now')),
('PAYPAL_SANDBOX', 0, date('now'), date('now')),
('PAYPAL_CURRENCY', 'customer', date('now'), date('now')),
('BANK_WIRE_CURRENCIES', '2,1', date('now'), date('now')),
('CHEQUE_CURRENCIES', '2,1', date('now'), date('now')),
('PRODUCTS_VIEWED_NBR', '2', date('now'), date('now')),
('BLOCK_CATEG_DHTML', '1', date('now'), date('now')),
('BLOCK_CATEG_MAX_DEPTH', '3', date('now'), date('now')),
('MANUFACTURER_DISPLAY_FORM', '1', date('now'), date('now')),
('MANUFACTURER_DISPLAY_TEXT', '1', date('now'), date('now')),
('MANUFACTURER_DISPLAY_TEXT_NB', '5', date('now'), date('now')),
('NEW_PRODUCTS_NBR', '5', date('now'), date('now')),
('PS_TOKEN_ENABLE', '1', date('now'), date('now'));

INSERT INTO `module` (`id_module`, `name`, `active`) VALUES
(1, 'homefeatured', 1),
(2, 'gsitemap', 1),
(3, 'cheque', 1),
(4, 'paypal', 1),
(5, 'editorial', 1),
(6, 'bankwire', 1),
(7, 'blockadvertising', 1),
(8, 'blockbestsellers', 1),
(9, 'blockcart', 1),
(10, 'blockcategories', 1),
(11, 'blockcurrencies', 1),
(12, 'blockinfos', 1),
(13, 'blocklanguages', 1),
(14, 'blockmanufacturer', 1),
(15, 'blockmyaccount', 1),
(16, 'blocknewproducts', 1),
(17, 'blockpaymentlogo', 1),
(18, 'blockpermanentlinks', 1),
(19, 'blocksearch', 1),
(20, 'blockspecials', 1),
(21, 'blocktags', 1),
(22, 'blockuserinfo', 1),
(23, 'blockvariouslinks', 1),
(24, 'blockviewed', 1);

INSERT INTO `hook_module` (`id_module`, `id_hook`, `position`) VALUES
(3, 1, 1),
(6, 1, 2),
(4, 1, 3),
(8, 2, 1),
(3, 4, 1),
(6, 4, 2),
(9, 6, 1),
(16, 6, 2),
(8, 6, 3),
(20, 6, 4),
(15, 7, 1),
(21, 7, 2),
(10, 7, 3),
(24, 7, 4),
(14, 7, 5),
(12, 7, 6),
(7, 7, 7),
(17, 7, 8),
(5, 8, 1),
(1, 8, 2),
(11, 14, 1),
(13, 14, 2),
(18, 14, 3),
(19, 14, 4),
(22, 14, 5),
(8, 19, 1),
(23, 21, 1);

INSERT INTO `carrier` (`id_carrier`, `id_tax`, `name`, `active`, `deleted`, `shipping_handling`) VALUES
(1, 0, 0, 1, 0, 0),
(2, 1, 'My carrier', 1, 0, 1);
INSERT INTO `carrier_lang` (`id_carrier`, `id_lang`, `delay`) VALUES
(1, 1, 'Pick up in-store'),
(1, 2, 'Retrait au magasin'),
(2, 1, 'Delivery next day!'),
(2, 2, 'Livraison le lendemain !');
INSERT INTO `carrier_zone` (`id_carrier`, `id_zone`) VALUES
(1, 1),
(2, 1),
(2, 2);

INSERT INTO `range_price` (`id_range_price`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, 0, 10000);
INSERT INTO `range_weight` (`id_range_weight`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, 0, 10000);
INSERT INTO `delivery` (`id_delivery`, `id_range_price`, `id_range_weight`, `id_carrier`, `id_zone`, `price`) VALUES
(1, NULL, 1, 2, 1, 5.00),
(2, NULL, 1, 2, 2, 5.00),
(4, 1, NULL, 2, 1, 5.00),
(5, 1, NULL, 2, 2, 5.00);

INSERT INTO `manufacturer` (`id_manufacturer`, `name`, `date_add`, `date_upd`) VALUES
(1, 'Apple', date('now'), date('now')),
(2, 'Shure', date('now'), date('now'));

INSERT INTO `supplier` (`id_supplier`, `name`, `date_add`, `date_upd`) VALUES
(1, 'AppleStore', date('now'), date('now')),
(2, 'Shure Online Store', date('now'), date('now'));

INSERT INTO `product` (`id_product`, `id_supplier`, `id_manufacturer`, `id_tax`, `id_category_default`, `id_color_default`, `on_sale`, `ean13`, `ecotax`, `quantity`, `price`, `wholesale_price`, `reduction_price`, `reduction_percent`, `reduction_from`, `reduction_to`, `reference`, `weight`, `out_of_stock`, `quantity_discount`, `active`, `date_add`, `date_upd`) VALUES
(1, 1, 1, 1, 2, 2, 0, '0', 0.00, 100, 174.7492, 92.0000, 0.00, 10, date('now'), date('now'), '', 0.5, 2, 0, 1, date('now'), date('now')),
(2, 1, 1, 1, 2, 0, 0, '0', 0.00, 12, 66.0535, 33.0000, 0.00, 0, date('now'), date('now'), '', 0, 2, 0, 1, date('now'), date('now')),
(5, 1, 1, 1, 4, 0, 0, '0', 0.00, 274, 1504.180602, 1000.000000, 0.00, 0, date('now'), date('now'), '', 1.36, 2, 0, 1, date('now'), date('now')),
(6, 1, 1, 1, 4, 0, 0, '0', 0.00, 250, 1170.568561, 0.000000, 0.00, 0, date('now'), date('now'), '', 0.75, 2, 0, 1, date('now'), date('now')),
(7, 0, 0, 1, 2, 0, 0, '', 0.00, 180, 241.638796, 200.000000, 0.00, 0, date('now'), date('now'), '', 0, 2, 0, 1, '2008-04-07 15:27:01', '2008-04-07 16:18:23'),
(8, 0, 0, 1, 3, 0, 0, '', 0.00, 1, 25.041806, 0.000000, 0.00, 0, date('now'), date('now'), '', 0, 2, 0, 1, '2008-04-07 17:09:52', '2008-04-07 17:28:56'),
(9, 2, 2, 1, 3, 0, 0, '', 0.00, 1, 124.581940, 0.000000, 0.00, 0, date('now'), date('now'), '', 0, 2, 0, 1, '2008-04-07 17:35:40', '2008-04-07 17:40:00');

INSERT INTO `product_lang` (`id_product`, `id_lang`, `description`, `description_short`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `availability`) VALUES
(1, 1, 'Look like a rock star. Your music says a lot about you. So should your iPod nano. A super-slim design says you always have room for music &mdash; up to 2,000 songs, in fact. Durable anodized aluminum says you won&rsquo;t let the rough and tumble of everyday life ruin your groove. And one of five colors says whatever you want. Choose your hue and make a statement. 						 						 						Carry a tune (or 2,000) 						Choose a 2GB, 4GB, or 8GB iPod nano and add a soundtrack to your life. Just use iTunes to import your CDs, shop for 99&cent; songs on the iTunes Store, then sync them to iPod nano. Possibly the world&rsquo;s coolest photo album, iPod nano holds up to 25,000 snapshots.2 iPod nano plays audiobooks and podcasts from the iTunes Store, too.', 'A thinner design. Five stylish colors. A brighter display. Up to 24 hours of battery life. Just about the only thing that hasn&rsquo;t changed is the name. In 2GB, 4GB, and 8GB models starting at $149, iPod nano puts up to 2,000 songs in your pocket.', 'ipod-nano', '', '', '', 'iPod Nano', 'En stock'),
(1, 2, 'Jouez les rock stars. La musique que vous écoutez en dit long sur votre personnalité. Il en va de même pour votre iPod nano. Un design ultra-fin laisse entendre que vous n''êtes jamais à court d''espace pour stocker des chansons : jusqu''à 2 000 très précisément. La résistance de l''aluminium anodisé indique que vous n''êtes pas du genre à laisser les aléas de la vie quotidienne vous faire perdre le rythme. Enfin, chacun des cinq coloris révèle tout ce qu''il vous plaît de montrer. Alors, choisissez votre teinte et affirmez votre différence.  				  				 				Prenez le large avec 2 000 chansons 				Choisissez un iPod nano de 2, 4 ou 8 Go et mettez votre vie en musique. Il vous suffit d''utiliser iTunes pour importer vos CD ou d''acheter des chansons à 0,99 &euro; sur iTunes Store, puis de les synchroniser sur votre iPod nano.', 'Un design plus fin. Cinq coloris tendance. Un écran plus lumineux. Jusqu''à 24 heures d''autonomie. La seule chose, ou presque, qui n''ait pas changé, c''est son nom', 'ipod-nano', '', '', '', 'iPod Nano', 'En stock'),
(2, 1, 'One size fits all. You know what they say about good things and small packages. But when something 1.62 inches long and about half an ounce holds up to 240 songs, &ldquo;good&rdquo; and &ldquo;small&rdquo; don&rsquo;t cut it. Especially when you can listen to your music for up to 12 continuous hours.2 In fact, iPod shuffle just may be the biggest thing in small. 						 							 							Ready to wear 							Clip it to your coin pocket. Clip it to your bag. No matter where you clip your skip-free iPod shuffle, you&rsquo;ll have instant access to music. In silver, pink, green, blue, and orange, iPod shuffle goes with everything. Put it on, turn it up, and turn some heads.', 'In five brilliant colors and just $79, the 1GB iPod shuffle lets you wear up to 240 songs on your sleeve. Or your lapel. Or your belt. Clip on iPod shuffle and wear it as a badge of musical devotion.', 'ipod-shuffle', '', '', '', 'iPod shuffle', 'En stock'),
(2, 2, 'Intégration parfaite. Vous savez ce qu''on dit : compacité rime rarement avec qualité. Mais quand un appareil de 4,1cm de long et d''environ 15 g peut contenir jusqu''à 240 chansons, ces arguments ne tiennent plus. Surtout quand on peut écouter jusqu''à 12 heures de musique non-stop.2 Malgré sa petite taille, iPod shuffle joue dans la cour des grands. 						 							 							Prêt-à-porter 							Accrochez-le à votre poche  de pantalon ou à votre sac. Quel que soit l''endroit o&ugrave; vous fixez votre iPod shuffle à lecture garantie sans saute, vous accédez instantanément à votre musique. En couleur argent, rose, vert, bleu ou orange, iPod shuffle se marie avec tout. Mettez-le, montez le son et faites des envieux.', 'Disponible en cinq coloris éclatants au prix de 79 &euro; seulement, le modèle iPod shuffle 1 Go vous permet de porter jusqu''à 240 chansoààns accrochées à votre manche. Ou au revers de votre veste. Ou encore à votre ceinture. ', 'ipod-shuffle', '', '', '', 'iPod shuffle', 'En stock'),
(5, 1, '<p>MacBook Air is nearly as thin as your index finger. Practically every detail that could be streamlined has been. Yet it still has a 13.3-inch widescreen LED display, full-size keyboard, and large multi-touch trackpad. It&rsquo;s incomparably portable without the usual ultraportable screen and keyboard compromises.</p><p>The incredible thinness of MacBook Air is the result of numerous size- and weight-shaving innovations. From a slimmer hard drive to strategically hidden I/O ports to a lower-profile battery, everything has been considered and reconsidered with thinness in mind.</p><p>MacBook Air is designed and engineered to take full advantage of the wireless world. A world in which 802.11n Wi-Fi is now so fast and so available, people are truly living untethered &mdash; buying and renting movies online, downloading software, and sharing and storing files on the web. </p>', 'MacBook Air is ultrathin, ultraportable, and ultra unlike anything else. But you don&rsquo;t lose inches and pounds overnight. It&rsquo;s the result of rethinking conventions. Of multiple wireless innovations. And of breakthrough design. With MacBook Air, mobile computing suddenly has a new standard.', 'macbook-air', '', '', '', 'MacBook Air', ''),
(5, 2, '<p>MacBook Air est presque aussi fin que votre index. Pratiquement tout ce qui pouvait être simplifié l''a été. Il n''en dispose pas moins d''un écran panoramique de 13,3 pouces, d''un clavier complet et d''un vaste trackpad multi-touch. Incomparablement portable il vous évite les compromis habituels en matière d''écran et de clavier ultra-portables.</p><p>L''incroyable finesse de MacBook Air est le résultat d''un grand nombre d''innovations en termes de réduction de la taille et du poids. D''un disque dur plus fin à des ports d''E/S habilement dissimulés en passant par une batterie plus plate, chaque détail a été considéré et reconsidéré avec la finesse à l''esprit.</p><p>MacBook Air a été con&ccedil;u et élaboré pour profiter pleinement du monde sans fil. Un monde dans lequel la norme Wi-Fi 802.11n est désormais si rapide et si accessible qu''elle permet véritablement de se libérer de toute attache pour acheter des vidéos en ligne, télécharger des logicééééiels, stocker et partager des fichiers sur le Web. </p>', 'MacBook Air est ultra fin, ultra portable et ultra diff&eacute;rent de tout le reste. Mais on ne perd pas des kilos et des centim&egrave;tres en une nuit. C''est le r&eacute;sultat d''une r&eacute;invention des normes. D''une multitude d''innovations sans fil. Et d''une r&eacute;volution dans le design. Avec MacBook Air, l''informatique mobile prend soudain une nouvelle dimension.', 'macbook-air', '', '', '', 'MacBook Air', ''),
(6, 1, 'Every MacBook has a larger hard drive, up to 250GB, to store growing media collections and valuable data.<br /><br />The 2.4GHz MacBook models now include 2GB of memory standard — perfect for running more of your favorite applications smoothly.', 'MacBook makes it easy to hit the road thanks to its tough polycarbonate case, built-in wireless technologies, and innovative MagSafe Power Adapter that releases automatically if someone accidentally trips on the cord.', 'macbook', '', '', '', 'MacBook', ''),
(6, 2, 'Chaque MacBook est équipé d''un disque dur plus spacieux, d''une capacité atteignant 250 Go, pour stocker vos collections multimédia en expansion et vos données précieuses.<br /><br />Le modèle MacBook à 2,4 GHz intègre désormais 2 Go de mémoire en standard. L''idéal pour exécuter en souplesse vos applications préférées.', 'MacBook vous offre la liberté de mouvement grâce à son boîtier résistant en polycarbonate, à ses technologies sans fil intégrées et à son adaptateur secteur MagSafe novateur qui se déconnecte automatiquement si quelqu''un se prend les pieds dans le fil.', 'macbook', '', '', '', 'MacBook', ''),
(7, 1, '<h3>Five new hands-on applications</h3>\r\n<p>View rich HTML email with photos as well as PDF, Word, and Excel attachments. Get maps, directions, and real-time traffic information. Take notes and read stock and weather reports.</p>\r\n<h3>Touch your music, movies, and more</h3>\r\n<p>The revolutionary Multi-Touch technology built into the gorgeous 3.5-inch display lets you pinch, zoom, scroll, and flick with your fingers.</p>\r\n<h3>Internet in your pocket</h3>\r\n<p>With the Safari web browser, see websites the way they were designed to be seen and zoom in and out with a tap.<sup>2</sup> And add Web Clips to your Home screen for quick access to favorite sites.</p>\r\n<h3>What&rsquo;s in the box</h3>\r\n<ul>\r\n<li><span></span>iPod touch</li>\r\n<li><span></span>Earphones</li>\r\n<li><span></span>USB 2.0 cable</li>\r\n<li><span></span>Dock adapter</li>\r\n<li><span></span>Polishing cloth</li>\r\n<li><span></span>Stand</li>\r\n<li><span></span>Quick Start guide</li>\r\n</ul>', '<ul>\r\n<li>Revolutionary Multi-Touch interface</li>\r\n<li>3.5-inch widescreen color display</li>\r\n<li>Wi-Fi (802.11b/g)</li>\r\n<li>8 mm thin</li>\r\n<li>Safari, YouTube, Mail, Stocks, Weather, Notes, iTunes Wi-Fi Music Store, Maps</li>\r\n</ul>', 'ipod-touch', '', '', '', 'iPod touch', ''),
(7, 2, '<h1>Titre 1</h1>\r\n<h2>Titre 2</h2>\r\n<h3>Titre 3</h3>\r\n<h4>Titre 4</h4>\r\n<h5>Titre 5</h5>\r\n<h6>Titre 6</h6>\r\n<ul>\r\n<li>UL</li>\r\n<li>UL</li>\r\n<li>UL</li>\r\n<li>UL</li>\r\n</ul>\r\n<ol>\r\n<li>OL</li>\r\n<li>OL</li>\r\n<li>OL</li>\r\n<li>OL</li>\r\n</ol>\r\n<p>paragraphe...</p>\r\n<p>paragraphe...</p>\r\n<p>paragraphe...</p>\r\n<table border="0">\r\n<thead> \r\n<tr>\r\n<th>th</th> <th>th</th> <th>th</th>\r\n</tr>\r\n</thead> \r\n<tbody>\r\n<tr>\r\n<td>td</td>\r\n<td>td</td>\r\n<td>td</td>\r\n</tr>\r\n<tr>\r\n<td>td</td>\r\n<td>td</td>\r\n<td>td</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>Cinq nouvelles applications sous la main</h3>\r\n<p>Consultez vos e-mails au format HTML enrichi, avec photos et pieces jointes au format PDF, Word et Excel. Obtenez des cartes, des itin&eacute;raires et des informations sur l''&eacute;tat de la circulation en temps r&eacute;el. R&eacute;digez des notes et consultez les cours de la Bourse et les bulletins m&eacute;t&eacute;o.</p>\r\n<h3>Touchez du doigt votre musique et vos vid&eacute;os. Entre autres.</h3>\r\n<p>La technologie multi-touch r&eacute;volutionnaire int&eacute;gr&eacute;e au superbe &eacute;cran de 3,5 pouces vous permet d''effectuer des zooms avant et arri&egrave;re, de faire d&eacute;filer et de feuilleter des pages &agrave; l''aide de vos seuls doigts.</p>\r\n<h3>Internet dans votre poche</h3>\r\n<p>Avec le navigateur Safari, vous pouvez consulter des sites web dans leur mise en page d''origine et effectuer un zoom avant et arri&egrave;re d''une simple pression sur l''&eacute;cran.</p>\r\n<h3>Contenu du coffret</h3>\r\n<ul>\r\n<li><span></span>iPod touch</li>\r\n<li><span></span>&Eacute;couteurs</li>\r\n<li><span></span>C&acirc;ble USB 2.0</li>\r\n<li><span></span>Adaptateur Dock</li>\r\n<li><span></span>Chiffon de nettoyage</li>\r\n<li><span></span>Support</li>\r\n<li><span></span>Guide de d&eacute;marrage rapide</li>\r\n</ul>\r\n<p>&nbsp;</p>', '<p>Interface multi-touch r&eacute;volutionnaire<br />&Eacute;cran panoramique couleur de 3,5 pouces<br />Wi-Fi (802.11b/g)<br />8 mm d''&eacute;paisseur<br />Safari, YouTube, iTunes Wi-Fi Music Store, Courrier, Cartes, Bourse, M&eacute;t&eacute;o, Notes</p>', 'ipod-touch', '', '', '', 'iPod touch', 'En stock'),
(8, 1, '<p>Lorem ipsum</p>', '<p>Lorem ipsum</p>', 'housse-portefeuille-en-cuir-belkin-pour-ipod-nano-noir-chocolat', '', '', '', 'Housse portefeuille en cuir Belkin pour iPod nano - Noir/Chocolat', ''),
(8, 2, '<p><strong>Caract&eacute;ristiques</strong></p>\r\n<li>Cuir doux r&eacute;sistant<br /> </li>\r\n<li>Acc&egrave;s au bouton Hold<br /> </li>\r\n<li>Fermeture magn&eacute;tique<br /> </li>\r\n<li>Acc&egrave;s au Dock Connector<br /> </li>\r\n<li>Prot&egrave;ge-&eacute;cran</li>', '<p>Cet &eacute;tui en cuir tendance assure une protection compl&egrave;te contre les &eacute;raflures et les petits al&eacute;as de la vie quotidienne. Sa conception &eacute;l&eacute;gante et compacte vous permet de glisser votre iPod directement dans votre poche ou votre sac &agrave; main.</p>', 'housse-portefeuille-en-cuir-ipod-nano-noir-chocolat', '', '', '', 'Housse portefeuille en cuir (iPod nano) - Noir/Chocolat', ''),
(9, 1, '<div class="product-overview-full">Using Hi-Definition MicroSpeakers to deliver full-range audio, the ergonomic and lightweight design of the SE210 earphones is ideal for premium on-the-go listening on your iPod or iPhone. They offer the most accurate audio reproduction from both portable and home stereo audio sources--for the ultimate in precision highs and rich low end. In addition, the flexible design allows you to choose the most comfortable fit from a variety of wearing positions. <br /> <br /> <strong>Features </strong> <br /> \r\n<ul>\r\n<li>Sound-isolating design </li>\r\n<li> Hi-Definition MicroSpeaker with a single balanced armature driver </li>\r\n<li> Detachable, modular cable so you can make the cable longer or shorter depending on your activity </li>\r\n<li> Connector compatible with earphone ports on both iPod and iPhone </li>\r\n</ul>\r\n<strong>Specifications </strong><br /> \r\n<ul>\r\n<li>Speaker type: Hi-Definition MicroSpeaker </li>\r\n<li> Frequency range: 25Hz-18.5kHz </li>\r\n<li> Impedance (1kHz): 26 Ohms </li>\r\n<li> Sensitivity (1mW): 114 dB SPL/mW </li>\r\n<li> Cable length (with extension): 18.0 in./45.0 cm (54.0 in./137.1 cm) </li>\r\n</ul>\r\n<strong>In the box</strong><br /> \r\n<ul>\r\n<li>Shure SE210 earphones </li>\r\n<li> Extension cable (36.0 in./91.4 cm) </li>\r\n<li> Three pairs foam earpiece sleeves (small, medium, large) </li>\r\n<li> Three pairs soft flex earpiece sleeves (small, medium, large) </li>\r\n<li> One pair triple-flange earpiece sleeves </li>\r\n<li> Carrying case </li>\r\n</ul>\r\nWarranty<br /> Two-year limited <br />(For details, please visit <br />www.shure.com/PersonalAudio/CustomerSupport/ProductReturnsAndWarranty/index.htm.) <br /><br /> Mfr. Part No.: SE210-A-EFS <br /><br />Note: Products sold through this website that do not bear the Apple Brand name are serviced and supported exclusively by their manufacturers in accordance with terms and conditions packaged with the products. Apple''s Limited Warranty does not apply to products that are not Apple-branded, even if packaged or sold with Apple products. Please contact the manufacturer directly for technical support and customer service.</div>', '<p>Evolved from personal monitor technology road-tested by pro musicians and perfected by Shure engineers, the lightweight and stylish SE210 delivers full-range audio that''s free from outside noise.</p>', 'ecouteurs-a-isolation-sonore-shure-se210-blanc', '', '', '', 'Shure SE210 Sound-Isolating Earphones for iPod and iPhone', ''),
(9, 2, '<p>Bas&eacute;s sur la technologie des moniteurs personnels test&eacute;e sur la route par des musiciens professionnels et perfectionn&eacute;e par les ing&eacute;nieurs Shure, les &eacute;couteurs SE210, l&eacute;gers et &eacute;l&eacute;gants, fournissent une sortie audio &agrave; gamme &eacute;tendue exempte de tout bruit externe.</p>\r\n<p><img src="http://store.apple.com/Catalog/fr/Images/TM255_screen1.jpg" border="0" /></p>\r\n<p><strong>Conception &agrave; isolation sonore <br /></strong>Les embouts &agrave; isolation sonore fournis bloquent plus de 90 % du bruit ambiant. Combin&eacute;s &agrave; un design ergonomique s&eacute;duisant et un c&acirc;ble modulaire, ils minimisent les intrusions du monde ext&eacute;rieur, vous permettant de vous concentrer sur votre musique. Con&ccedil;us pour les amoureux de la musique qui souhaitent faire &eacute;voluer leur appareil audio portable, les &eacute;couteurs SE210 vous permettent d''emmener la performance avec vous. <br /> <br /><strong>Micro-transducteur haute d&eacute;finition <br /></strong>D&eacute;velopp&eacute;s pour une &eacute;coute de qualit&eacute; sup&eacute;rieure en d&eacute;placement, les &eacute;couteurs SE210 utilisent un seul transducteur &agrave; armature &eacute;quilibr&eacute;e pour b&eacute;n&eacute;ficier d''une gamme audio &eacute;tendue. Le r&eacute;sultat ? Un confort d''&eacute;coute &eacute;poustouflant qui restitue tous les d&eacute;tails d''un spectacle live.</p>\r\n<p><strong>Le kit universel Deluxe comprend les &eacute;l&eacute;ments suivants : <br /></strong>- <strong><em>Embouts &agrave; isolation sonore</em></strong> <br />Les embouts &agrave; isolation sonore inclus ont un double r&ocirc;le : bloquer les bruits ambiants et garantir un maintien et un confort personnalis&eacute;s. Comme chaque oreille est diff&eacute;rente, le kit universel Deluxe comprend trois tailles (S, M, L) d''embouts mousse et flexibles. Choisissez la taille et le style d''embout qui vous conviennent le mieux : une bonne &eacute;tanch&eacute;it&eacute; est un facteur cl&eacute; pour optimiser l''isolation sonore et la r&eacute;ponse des basses, ainsi que pour accro&icirc;tre le confort en &eacute;coute prolong&eacute;e.<br /><br />- <em><strong>C&acirc;ble modulaire</strong></em> <br />En se basant sur les commentaires de nombreux utilisateurs, les ing&eacute;nieurs de Shure ont d&eacute;velopp&eacute; une solution de c&acirc;ble d&eacute;tachable pour permettre un degr&eacute; de personnalisation sans pr&eacute;c&eacute;dent. Le c&acirc;ble de 1 m&egrave;tre fourni vous permet d''adapter votre confort en fonction de l''activit&eacute; et de l''application.<br /> <br />- <em><strong>&Eacute;tui de transport</strong></em> <br />Outre les embouts &agrave; isolation sonore et le c&acirc;ble modulaire, un &eacute;tui de transport compact et r&eacute;sistant est fourni avec les &eacute;couteurs SE210 pour vous permettre de ranger vos &eacute;couteurs de mani&egrave;re pratique et sans encombres.<br /> <br />- <strong><em>Garantie limit&eacute;e de deux ans <br /></em></strong>Chaque solution SE210 achet&eacute;e est couverte par une garantie pi&egrave;ces et main-d''&oelig;uvre de deux ans.<br /><br /><strong>Caract&eacute;ristiques techniques</strong></p>\r\n<ul>\r\n<li> Type de transducteur : micro-transducteur haute d&eacute;finition<br /></li>\r\n<li> Sensibilit&eacute; (1 mW) : pression acoustique de 114 dB/mW<br /></li>\r\n<li> Imp&eacute;dance (&agrave; 1 kHz) : 26 W<br /></li>\r\n<li> Gamme de fr&eacute;quences : 25 Hz &ndash; 18,5 kHz<br /></li>\r\n<li> Longueur de c&acirc;ble / avec rallonge : 45 cm / 136 cm<br /></li>\r\n</ul>\r\n<p><strong>Contenu du coffret<br /></strong></p>\r\n<ul>\r\n<li> &Eacute;couteurs Shure SE210<br /></li>\r\n<li> Kit universel Deluxe (embouts &agrave; isolation sonore, c&acirc;ble modulaire, &eacute;tui de transport)</li>\r\n</ul>', '<p>Les &eacute;couteurs &agrave; isolation sonore ergonomiques et l&eacute;gers offrent la reproduction audio la plus fid&egrave;le en provenance de sources audio st&eacute;r&eacute;o portables ou de salon.</p>', 'ecouteurs-a-isolation-sonore-shure-se210', '', '', '', 'Écouteurs à isolation sonore Shure SE210', '');

INSERT INTO `category` VALUES (2, 1, 1, 1, date('now'), date('now'));
INSERT INTO `category` VALUES (3, 1, 1, 1, date('now'), date('now'));
INSERT INTO `category` VALUES (4, 1, 1, 1, date('now'), date('now'));

INSERT INTO `category_lang` VALUES
(2, 1, 'iPods', 'Now that you can buy movies from the iTunes Store and sync them to your iPod, the whole world is your theater.', 'music-ipods', '', '', ''),
(2, 2, 'iPods', 'Il est temps, pour le meilleur lecteur de musique, de remonter sur scène pour un rappel. Avec le nouvel iPod, le monde est votre scène.', 'musique-ipods', '', '', ''),
(3, 1, 'Accessories', 'Wonderful accessories for your iPod', 'accessories-ipod', '', '', ''),
(3, 2, 'Accessoires', 'Tous les accessoires à la mode pour votre iPod', 'accessoires-ipod', '', '', ''),
(4, 1, 'Laptops', 'The latest Intel processor, a bigger hard drive, plenty of memory, and even more new features all fit inside just one liberating inch. The new Mac laptops have the performance, power, and connectivity of a desktop computer. Without the desk part.', 'laptops', 'Apple laptops', 'Apple laptops MacBook Air', 'Powerful and chic Apple laptops'),
(4, 2, 'Portables', 'Le tout dernier processeur Intel, un disque dur plus spacieux, de la mémoire à profusion et d''autres nouveautés. Le tout, dans à peine 2,59 cm qui vous libèrent de toute entrave. Les nouveaux portables Mac réunissent les performances, la puissance et la connectivité d''un ordinateur de bureau. Sans la partie bureau.', 'portables-apple', 'Portables Apple', 'portables apple macbook air', 'portables apple puissants et design');

INSERT INTO `category_product` (`id_category`, `id_product`, `position`) VALUES 
(4, 5, 1),
(2, 2, 2),
(2, 1, 1),
(1, 6, 4),
(1, 1, 1),
(1, 2, 2),
(2, 7, 3),
(1, 7, 5),
(3, 8, 0),
(4, 6, 2),
(3, 9, 1);

INSERT INTO `attribute_group` (`id_attribute_group`, `is_color_group`) VALUES
(1, 0),
(2, 1),
(3, 0);

INSERT INTO `attribute_group_lang` VALUES
(1, 1, 'Disk space', 'Disk space'),
(1, 2, 'Capacité', 'Capacité'),
(2, 1, 'Color', 'Color'),
(2, 2, 'Couleur', 'Couleur'),
(3, 1, 'ICU', 'Processor'),
(3, 2, 'ICU', 'Processeur');

INSERT INTO `attribute` (`id_attribute`, `id_attribute_group`) VALUES
(1, 1),
(2, 1);
INSERT INTO `attribute` (`id_attribute`, `id_attribute_group`, `color`) VALUES
(3, 2, '#D2D6D5'),
(4, 2, '#008CB7'),
(5, 2, '#F3349E'),
(6, 2, '#93D52D'),
(7, 2, '#FD9812');
INSERT INTO `attribute` (`id_attribute`, `id_attribute_group`) VALUES
(8, 1),
(9, 1),
(10, 3),
(11, 3),
(12, 1),
(13, 1),
(14, 2);
INSERT INTO `attribute` (`id_attribute`, `id_attribute_group`, `color`) VALUES 
(15, 1, ''),
(16, 1, ''),
(17, 1, '');

INSERT INTO `attribute_lang` VALUES
(1, 1, '2GB'),
(1, 2, '2Go'),
(2, 1, '4GB'),
(2, 2, '4Go'),
(3, 1, 'Metal'),
(3, 2, 'Metal'),
(4, 1, 'Blue'),
(4, 2, 'Bleu'),
(5, 1, 'Pink'),
(5, 2, 'Rose'),
(6, 1, 'Green'),
(6, 2, 'Vert'),
(7, 1, 'Orange'),
(7, 2, 'Orange'),
(8, 1, 'Optional 64GB solid-state drive'),
(8, 2, 'Disque dur SSD (solid-state drive) de 64 Go '),
(9, 1, '80GB Parallel ATA Drive @ 4200 rpm'),
(9, 2, 'Disque dur PATA de 80 Go à 4 200 tr/min'),
(10, 1, '1.60GHz Intel Core 2 Duo'),
(10, 2, 'Intel Core 2 Duo à 1,6 GHz'),
(11, 1, '1.80GHz Intel Core 2 Duo'),
(11, 2, 'Intel Core 2 Duo à 1,8 GHz'),
(12, 1, '80GB: 20,000 Songs'),
(12, 2, '80 Go : 20 000 chansons'),
(13, 1, '160GB: 40,000 Songs'),
(13, 2, '160 Go : 40 000 chansons'),
(14, 2, 'Noir'),
(14, 1, 'Black'),
(15, 1, '8Go'),
(15, 2, '8Go'),
(16, 1, '16Go'),
(16, 2, '16Go'),
(17, 1, '32Go'),
(17, 2, '32Go');

INSERT INTO `product_attribute` (`id_product_attribute`, `id_image`, `id_product`, `reference`, `ean13`, `price`, `ecotax`, `quantity`, `weight`, `default_on`) VALUES
(1, 4, 1, '', '', 0.00, 0.00, 22, 0, 0),
(2, 4, 1, '', '', 50.00, 0.00, 200, 0, 0),
(3, 2, 1, '', '', 0.00, 0.00, 40, 0, 0),
(4, 1, 1, '', '', 50.00, 0.00, 10, 0, 1),
(5, 3, 1, '', '', 0.00, 0.00, 31, 0, 0),
(6, 3, 1, '', '', 60.00, 0.00, 34, 0, 0),
(7, 6, 2, '', '', 0.00, 0.00, 10, 0, 0),
(8, 9, 2, '', '', 0.00, 0.00, 20, 0, 0),
(9, 5, 2, '', '', 0.00, 0.00, 30, 0, 1),
(10, 8, 2, '', '', 0.00, 0.00, 40, 0, 0),
(11, 7, 2, '', '', 0.00, 0.00, 32, 0, 0),
(12, 0, 5, '', '', 899.00, 0.00, 100, 0, 0),
(13, 0, 5, '', '', 0.00, 0.00, 99, 0, 1),
(14, 0, 5, '', '', 270.00, 0.00, 50, 0, 0),
(15, 0, 5, '', '', 1169.00, 0.00, 25, 0, 0),
(23, 0, 7, '', '', 180.00, 0.00, 70, 0, 0),
(22, 0, 7, '', '', 90.00, 0.00, 60, 0, 0),
(19, 0, 7, '', '', 0.00, 0.00, 50, 0, 1);

INSERT INTO `product_attribute_combination` (`id_attribute`, `id_product_attribute`) VALUES
(1, 1),
(1, 3),
(1, 5),
(2, 2),
(2, 4),
(2, 6),
(3, 4),
(3, 9),
(4, 1),
(4, 2),
(4, 7),
(5, 5),
(5, 6),
(5, 10),
(6, 3),
(6, 8),
(7, 11),
(3, 12),
(9, 12),
(10, 12),
(3, 13),
(8, 13),
(10, 13),
(3, 14),
(9, 14),
(11, 14),
(3, 15),
(8, 15),
(11, 15),
(15, 19),
(16, 22),
(17, 23);

INSERT INTO `feature` (`id_feature`) VALUES
(1), (2), (3), (4), (5);

INSERT INTO `feature_lang` (`id_feature`, `id_lang`, `name`) VALUES
(1, 1, 'Height'), (1, 2, 'Hauteur'),
(2, 1, 'Width'), (2, 2, 'Largeur'),
(3, 1, 'Depth'), (3, 2, 'Profondeur'),
(4, 1, 'Weight'), (4, 2, 'Poids'),
(5, 1, 'Headphone'), (5, 2, 'Prise casque');

INSERT INTO `feature_product` (`id_feature`, `id_product`, `id_feature_value`) VALUES
(1, 1, 11),
(1, 2, 15),
(2, 1, 12),
(2, 2, 16),
(3, 1, 14),
(3, 2, 18),
(4, 1, 13),
(4, 2, 17),
(5, 1, 10),
(5, 2, 10),
(3, 7, 26),
(5, 7, 9),
(4, 7, 25),
(2, 7, 24),
(1, 7, 23);

INSERT INTO `feature_value` (`id_feature_value`, `id_feature`, `custom`) VALUES
(11, 1, 1),
(15, 1, 1),
(12, 2, 1),
(16, 2, 1),
(14, 3, 1),
(18, 3, 1),
(13, 4, 1),
(17, 4, 1),
(26, 3, 1),
(25, 4, 1),
(24, 2, 1),
(23, 1, 1);

INSERT INTO `feature_value` (`id_feature_value`, `id_feature`, `custom`) VALUES
(9, 5, NULL), (10, 5, NULL);

INSERT INTO `feature_value_lang` (`id_feature_value`, `id_lang`, `value`) VALUES
(13, 1, '49.2 grams'),
(13, 2, '49,2 grammes'),
(12, 2, '52,3 mm'),
(12, 1, '52.3 mm'),
(11, 2, '69,8 mm'),
(11, 1, '69.8 mm'),
(17, 2, '15,5 g'),
(17, 1, '15.5 g'),
(16, 2, '41,2 mm'),
(16, 1, '41.2 mm'),
(15, 2, '27,3 mm'),
(15, 1, '27.3 mm'),
(9, 1, 'Jack stereo'),
(9, 2, 'Jack stéréo'),
(10, 1, 'Mini-jack stereo'),
(10, 2, 'Mini-jack stéréo'),
(14, 1, '6,5 mm'),
(14, 2, '6,5 mm'),
(18, 1, '10,5 mm (clip compris)'),
(18, 2, '10,5 mm (clip compris)'),
(26, 2, '8mm'),
(26, 1, '8mm'),
(25, 2, '120g'),
(25, 1, '120g'),
(24, 2, '70mm'),
(24, 1, '70mm'),
(23, 2, '110mm'),
(23, 1, '110mm');

INSERT INTO `image` (`id_image`, `id_product`, `position`, `cover`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 0),
(3, 1, 3, 0),
(4, 1, 4, 0),
(5, 2, 1, 1),
(6, 2, 2, 0),
(7, 2, 3, 0),
(8, 2, 4, 0),
(9, 2, 5, 0),
(15, 5, 1, 1),
(16, 5, 2, 0),
(17, 5, 3, 0),
(18, 6, 4, 0),
(19, 6, 5, 0),
(20, 6, 1, 1),
(24, 7, 1, 1),
(33, 8, 1, 1),
(27, 7, 3, 0),
(26, 7, 2, 0),
(29, 7, 4, 0),
(30, 7, 5, 0),
(32, 7, 6, 0),
(36, 9, 1, 1);

INSERT INTO `image_lang` (`id_image`, `id_lang`, `legend`) VALUES
(1, 1, 'ipod-nano-alu'),
(1, 2, 'ipod-nano-alu'),
(2, 1, 'ipod-nano-green'),
(2, 2, 'ipod-nano-vert'),
(3, 1, 'ipod-nano-pink'),
(3, 2, 'ipod-nano-rose'),
(4, 1, 'ipod-nano-blue'),
(4, 2, 'ipod-nano-bleu'),
(5, 1, 'ipod-shuffle-metal'),
(5, 2, 'ipod-shuffle-metal'),
(6, 1, 'ipod-shuffle-blue'),
(6, 2, 'ipod-shuffle-bleu'),
(7, 1, 'ipod-shuffle-orange'),
(7, 2, 'ipod-shuffle-orange'),
(8, 1, 'ipod-shuffle-pink'),
(8, 2, 'ipod-shuffle-rose'),
(9, 1, 'ipod-shuffle-green'),
(9, 2, 'ipod-shuffle-vert'),
(10, 1, 'luxury-cover-for-ipod-video'),
(10, 2, 'housse-luxe-pour-ipod-video'),
(11, 1, 'cover'),
(11, 2, 'housse'),
(12, 1, 'myglove-ipod-nano'),
(12, 2, 'myglove-ipod-nano'),
(13, 1, 'myglove-ipod-nano'),
(13, 2, 'myglove-ipod-nano'),
(14, 1, 'myglove-ipod-nano'),
(14, 2, 'myglove-ipod-nano'),
(15, 1, 'MacBook Air'),
(15, 2, 'macbook-air-1'),
(16, 1, 'MacBook Air'),
(16, 2, 'macbook-air-2'),
(17, 1, 'MacBook Air'),
(17, 2, 'macbook-air-3'),
(18, 1, 'MacBook Air'),
(18, 2, 'macbook-air-4'),
(19, 1, 'MacBook Air'),
(19, 2, 'macbook-air-5'),
(20, 1, ' MacBook Air SuperDrive'),
(20, 2, 'superdrive-pour-macbook-air-1'),
(24, 2, 'iPod touch'),
(24, 1, 'iPod touch'),
(33, 1, 'housse-portefeuille-en-cuir'),
(26, 1, 'iPod touch'),
(26, 2, 'iPod touch'),
(27, 1, 'iPod touch'),
(27, 2, 'iPod touch'),
(29, 1, 'iPod touch'),
(29, 2, 'iPod touch'),
(30, 1, 'iPod touch'),
(30, 2, 'iPod touch'),
(32, 1, 'iPod touch'),
(32, 2, 'iPod touch'),
(33, 2, 'housse-portefeuille-en-cuir-ipod-nano'),
(36, 2, 'Écouteurs à isolation sonore Shure SE210'),
(36, 1, 'Shure SE210 Sound-Isolating Earphones for iPod and iPhone');

INSERT INTO `tag` (`id_tag`, `id_lang`, `name`) VALUES
(5, 1, 'apple'),
(6, 2, 'ipod'),
(7, 2, 'nano'),
(8, 2, 'apple'),
(18, 2, 'shuffle'),
(19, 2, 'macbook'),
(20, 2, 'macbookair'),
(21, 2, 'air'),
(22, 1, 'superdrive'),
(27, 2, 'marche'),
(26, 2, 'casque'),
(25, 2, 'écouteurs'),
(24, 2, 'ipod touch tacticle'),
(23, 1, 'Ipod touch');

INSERT INTO `product_tag` (`id_product`, `id_tag`) VALUES
(1, 2),
(1, 6),
(1, 7),
(1, 8),
(2, 6),
(2, 18),
(5, 8),
(5, 19),
(5, 20),
(5, 21),
(6, 5),
(6, 22),
(7, 23),
(7, 24),
(9, 25),
(9, 26),
(9, 27);

INSERT INTO `alias` (`alias`, `search`, `active`, `id_alias`) VALUES ('piod', 'ipod', 1, 4);
INSERT INTO `alias` (`alias`, `search`, `active`, `id_alias`) VALUES ('ipdo', 'ipod', 1, 3);

/* Double lang for all languages */

INSERT IGNORE INTO `carrier_lang` (`id_carrier`, `id_lang`, `delay`)
    (SELECT `id_carrier`, id_lang, (SELECT tl.`delay`
        FROM `carrier_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_carrier`=`carrier`.`id_carrier`)
    FROM `lang` CROSS JOIN `carrier`);

INSERT IGNORE INTO `attribute_group_lang` (`id_attribute_group`, `id_lang`, `name`, `public_name`)
    (SELECT `id_attribute_group`, id_lang, (SELECT tl.`name`
        FROM `attribute_group_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_attribute_group`=`attribute_group`.`id_attribute_group`),
		(SELECT tl.`public_name`
        FROM `attribute_group_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_attribute_group`=`attribute_group`.`id_attribute_group`)
    FROM `lang` CROSS JOIN `attribute_group`);

INSERT IGNORE INTO `attribute_lang` (`id_attribute`, `id_lang`, `name`)
    (SELECT `id_attribute`, id_lang, (SELECT tl.`name`
        FROM `attribute_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_attribute`=`attribute`.`id_attribute`)
    FROM `lang` CROSS JOIN `attribute`);

/* products */
INSERT IGNORE INTO `product_lang` (`id_product`, `id_lang`, `description`, `description_short`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `availability`)
    (SELECT `id_product`, id_lang,
	(SELECT tl.`description`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`),
	(SELECT tl.`description_short`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`),
	(SELECT tl.`link_rewrite`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`),
	(SELECT tl.`meta_description`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`),
	(SELECT tl.`meta_keywords`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`),
	(SELECT tl.`meta_title`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`),
	(SELECT tl.`name`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`),
	(SELECT tl.`availability`
        FROM `product_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_product`=`product`.`id_product`)
	FROM `lang` CROSS JOIN `product`);
	
/* categories */
INSERT IGNORE INTO `category_lang` (`id_category`, `id_lang`, `description`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`)
    (SELECT `id_category`, id_lang,
	(SELECT tl.`description`
        FROM `category_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_category`=`category`.`id_category`),
	(SELECT tl.`link_rewrite`
        FROM `category_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_category`=`category`.`id_category`),
	(SELECT tl.`meta_description`
        FROM `category_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_category`=`category`.`id_category`),
	(SELECT tl.`meta_keywords`
        FROM `category_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_category`=`category`.`id_category`),
	(SELECT tl.`meta_title`
        FROM `category_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_category`=`category`.`id_category`),
	(SELECT tl.`name`
        FROM `category_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_category`=`category`.`id_category`)
	FROM `lang` CROSS JOIN `category`);

INSERT IGNORE INTO `feature_lang` (`id_feature`, `id_lang`, `name`)
    (SELECT `id_feature`, id_lang, (SELECT tl.`name`
        FROM `feature_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_feature`=`feature`.`id_feature`)
    FROM `lang` CROSS JOIN `feature`);

INSERT IGNORE INTO `feature_value_lang` (`id_feature_value`, `id_lang`, `value`)
    (SELECT `id_feature_value`, id_lang, (SELECT tl.`value`
        FROM `feature_value_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_feature_value`=`feature_value`.`id_feature_value`)
    FROM `lang` CROSS JOIN `feature_value`);

INSERT IGNORE INTO `image_lang` (`id_image`, `id_lang`, `legend`)
    (SELECT `id_image`, id_lang, (SELECT tl.`legend`
        FROM `image_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_image`=`image`.`id_image`)
    FROM `lang` CROSS JOIN `image`);

INSERT IGNORE INTO `manufacturer_lang` (`id_manufacturer`, `id_lang`, `description`)
    (SELECT `id_manufacturer`, id_lang, (SELECT tl.`description`
        FROM `manufacturer_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_manufacturer`=`manufacturer`.`id_manufacturer`)
    FROM `lang` CROSS JOIN `manufacturer`);

INSERT IGNORE INTO `supplier_lang` (`id_supplier`, `id_lang`, `description`)
    (SELECT `id_supplier`, id_lang, (SELECT tl.`description`
        FROM `supplier_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_supplier`=`supplier`.`id_supplier`)
    FROM `lang` CROSS JOIN `supplier`);

INSERT IGNORE INTO `tax_lang` (`id_tax`, `id_lang`, `name`)
    (SELECT `id_tax`, id_lang, (SELECT tl.`name`
        FROM `tax_lang` tl
        WHERE tl.`id_lang` = (SELECT c.`value`
            FROM `configuration` c
            WHERE c.`name` = 'PS_LANG_DEFAULT' LIMIT 1) AND tl.`id_tax`=`tax`.`id_tax`)
    FROM `lang` CROSS JOIN `tax`);

