// test.c (creates a single rose.bmp image for testing)
#include <wand/MagickWand.h>

int main()
{

    int arg_count;
    char *args[] = { "-size", "100x100", "xc:red",
                     "(", "logo:", "-rotate", "-90", ")",
                     "+append", "show:", NULL };

    for(arg_count = 0; args[arg_count] != (char *) NULL; arg_count++);

    MagickWandGenesis();
    MagickWand *test = NewMagickWand();
    MagickWriteImage(test, "logo.bmp");
    DestroyMagickWand(test);
    MagickWandTerminus();

    MagickWand *mw = NULL;
    MagickWandGenesis();

    mw = NewMagickWand();
    MagickSetSize(mw,200,200);
    MagickReadImage(mw,"plasma:red-yellow");
    MagickWriteImage(mw, "plasma.bmp");
    if(mw)mw = DestroyMagickWand(mw);


    mw = NewMagickWand();
    MagickSetSize(mw,200,200);
    MagickReadImage(mw,"xc:");
    MagickAddNoiseImage(mw,RandomNoise,1.2);
    MagickSetImageVirtualPixelMethod(mw,TileVirtualPixelMethod);
    MagickBlurImage(mw,0,10);
    MagickNormalizeImage(mw);
    MagickWriteImage(mw, "test.bmp");
    if(mw)mw = DestroyMagickWand(mw);

    MagickWandTerminus();
}
